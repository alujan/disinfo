package es.uji.kirbys.entities;

import java.util.ArrayList;
import java.util.List;

public class Author {

	private String name;
	private String pseudonym;
	private List <Comic> comics = new ArrayList<Comic>();
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPseudonym() {
		return pseudonym;
	}
	public void setPseudonym(String pseudonym) {
		this.pseudonym = pseudonym;
	}
	public List<Comic> getComics() {
		return comics;
	}
	public void setComics(List<Comic> comics) {
		this.comics = comics;
	}
	
	
	
}
