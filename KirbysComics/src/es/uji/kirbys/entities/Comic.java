package es.uji.kirbys.entities;

import java.util.Date;
import java.util.List;

public class Comic {
	private int code;
	private String title;
	private int periodicity;
	private String genre;
	private Date startOfPublishing;
	private Date endOfPublishing;
	private Publisher publisher;
	private List<Author> authors;
	
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getPeriodicity() {
		return periodicity;
	}
	public void setPeriodicity(int periodicity) {
		this.periodicity = periodicity;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public Date getStartOfPublishing() {
		return startOfPublishing;
	}
	public void setStartOfPublishing(Date startOfPublishing) {
		this.startOfPublishing = startOfPublishing;
	}
	public Date getEndOfPublishing() {
		return endOfPublishing;
	}
	public void setEndOfPublishing(Date endOfPublishing) {
		this.endOfPublishing = endOfPublishing;
	}
	
	public Publisher getPublisher() {
		return publisher;
	}
	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	public List<Author> getAuthors() {
		return authors;
	}
	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}
	
	
	
}
