package es.uji.kirbys.entities;

public class NewCopy extends Publication {
	
	private double price;
	private int stock;
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	
}
