package es.uji.kirbys.entities;

import java.util.ArrayList;
import java.util.List;

public class Publication {
	
	private int number;
	private List<Sale> sales = new ArrayList<Sale>();
	private Comic comic;
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
