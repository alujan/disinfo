package es.uji.kirbys.entities;

import java.util.HashSet;
import java.util.Set;

public class Publisher {

	private String nif;
	private String address;
	private int telephone;
	private Set<Comic> comics = new HashSet<Comic>();

	public Set<Comic> getComics() {
		return comics;
	}

	public void setComics(Set<Comic> comics) {
		this.comics = comics;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}

}
