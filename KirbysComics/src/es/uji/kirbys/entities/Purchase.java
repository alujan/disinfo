package es.uji.kirbys.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Purchase {

	private int code;
	private Date date;
	private List<SecondHandCopy> sHandCopies = new ArrayList<SecondHandCopy>();
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public List<SecondHandCopy> getsHandCopies() {
		return sHandCopies;
	}
	public void setsHandCopies(List<SecondHandCopy> sHandCopies) {
		this.sHandCopies = sHandCopies;
	}
	
	
	
	
}
