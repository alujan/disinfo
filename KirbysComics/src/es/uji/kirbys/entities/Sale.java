package es.uji.kirbys.entities;

import java.util.Date;

public class Sale {
	
	private int code;
	private Date date;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

}
