package es.uji.kirbys.entities;

public class SecondHandCopy {

	private String preservationStatus;
	private int stock;
	private double price;
	
	public String getPreservationStatus() {
		return preservationStatus;
	}
	public void setPreservationStatus(String preservationStatus) {
		this.preservationStatus = preservationStatus;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

}