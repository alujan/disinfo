<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Club Esportiu - Crear nou soci</title>
</head>
<body>
	<h2>Nou soci</h2>
	<form:form method="post" modelAttribute="soci">
		<table>
			<tr>
				<td><form:label path="nomSoci">Nom</form:label></td>
				<td><form:input path="nomSoci" /></td>
			</tr>
			￼￼￼￼
			<tr>
				<td><form:label path="domicili">Domicili</form:label></td>
				<td><form:input path="domicili" /></td>
			</tr>
			<tr>
				<td><form:label path="dataNaixement">Data</form:label></td>
				<td><form:input path="dataNaixement" /></td>
			</tr>
			<tr>
				<td colspan="2">
				<input type="submit" value="Afegeix soci" />
				</td>
			</tr>
		</table>
	</form:form>

</body>
</html>