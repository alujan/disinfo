<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<title>Gestionar Socis</title>
</head>
<body>
	<h1>Llista de Socis</h1>
	<table>
		<tr>
			<th>Número</th>
			<th>Nom</th>
			<th>Domicili</th>
			<th>Data de naixement</th>
			<th>Data de registre</th>
		</tr>
		<c:forEach items="${socis}" var="soci">
			<tr>
				<td>${soci.numSoci}</td>
				<td>${soci.nomSoci}</td>
				<td>${soci.domicili}</td>
				<td>${soci.dataNaixement}</td>
				<td>${soci.dataRegistre}</td>
				<td><a href="update/${soci.numSoci}.html">Edita</a>
				<td><a href="delete/${soci.numSoci}.html">Esborra</a>
			</tr>
		</c:forEach>
	</table>
	<a href="add.html">Afegeix soci</a>
</body>
</html>