package es.uji.ei1027.clubesportiu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import es.uji.ei1027.clubesportiu.tractament.Tractament;

@Controller
public class ProvaController {

	// @RequestMapping("/prova")
	// public String provaWeb(ModelMap model){
	//
	// String message= "Provant la Web del Club Esportiu";
	// model.addAttribute("message", message);
	// return "prova";
	// }
	
	@Autowired
	Tractament tractament;
	
	public Tractament getTractament(){
		return tractament;
	}
	
	public void setTractament(Tractament tractament){
		this.tractament=tractament;
	}
	
	
	@RequestMapping("/prova_id")
	public ModelAndView provaID(ModelMap model){
		return new ModelAndView("prova","message","Hola "+ tractament.getTractament());
	}
	
	@RequestMapping("/prova")
	public ModelAndView provaWeb(ModelMap model) {
		String message = "Provant la Web del Club Esportiu";
		return new ModelAndView("prova", "message", message);

	}

}
