package es.uji.ei1027.clubesportiu.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.uji.ei1027.clubesportiu.dao.SociDao;
import es.uji.ei1027.clubesportiu.model.Soci;

@Controller
@RequestMapping("/soci")
public class SociController {

	@Autowired
	private SociDao sociDao;

	
	public void setSociDao(SociDao sociDao) {
		this.sociDao = sociDao;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@RequestMapping("/list")
	public ModelAndView listSocis() {
		return new ModelAndView("soci/list", "socis", sociDao.listSocis());
	}
	
	@RequestMapping("/add")
	public ModelAndView addSoci(){
		return new ModelAndView("soci/add", "soci", new Soci());
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String processAddSubmit(@ModelAttribute("soci") Soci soci, BindingResult bindingResult){
		
		if(bindingResult.hasErrors())
			return "soci/add";
		soci.setDataRegistre(new Date());
		sociDao.addSoci(soci);
		return "redirect:list.html";
		
	}
	
	@RequestMapping(value="/update/{numSoci}", method=RequestMethod.GET)
	public ModelAndView editSoci(@PathVariable int numSoci){
		return new ModelAndView("soci/update", "soci", sociDao.getSociById(numSoci));
	}
	
	
	@RequestMapping(value="/update/{numSoci}", method=RequestMethod.POST)
	public String processUpdateSubmit(@PathVariable int numSoci, @ModelAttribute ("soci") Soci soci, BindingResult bindingResult){
		
		if(bindingResult.hasErrors())
			return "soci/update";
		
		sociDao.updateSoci(soci);
		return "redirect:../list.html";
	}
	
	@RequestMapping(value="/delete/{numSoci}", method=RequestMethod.GET)
	public String deleteSoci(@PathVariable int numSoci){
		sociDao.deleteSoci(numSoci);
		return "redirect:../list.html";
	}
	
}
