package es.uji.ei1027.clubesportiu.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.ei1027.clubesportiu.model.Soci;

@Repository
public class HibernateSociDao implements SociDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Soci> listSocis() {
		return getCurrentSession().createQuery("from Soci").list();
	}

	@Override
	@Transactional
	public void addSoci(Soci soci) {
		getCurrentSession().save(soci);
	}

	@Override
	@Transactional
	public void updateSoci(Soci unSoci) {
		getCurrentSession().update(unSoci);
	}

	@Override
	@Transactional
	public void deleteSoci(int numSoci) {
		getCurrentSession().delete(getSociById(numSoci));
	}

	@Override
	@Transactional
	public Soci getSociById(int numSoci) {
		Query query = getCurrentSession().createQuery("from Soci where numSoci=:numSoci");
		query.setParameter("numSoci", numSoci);
		return (Soci)query.uniqueResult();
	}
	
}
