package es.uji.ei1027.clubesportiu.dao;

import java.util.List;

import es.uji.ei1027.clubesportiu.model.Soci;

public interface SociDao {

	List<Soci> listSocis();

	Soci getSociById(int numSoci);

	void addSoci(Soci soci);

	void updateSoci(Soci soci);

	void deleteSoci(int numSoci);
}
