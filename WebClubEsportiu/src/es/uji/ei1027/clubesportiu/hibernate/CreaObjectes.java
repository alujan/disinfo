package es.uji.ei1027.clubesportiu.hibernate;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.hibernate.Session;

import es.uji.ei1027.clubesportiu.model.Soci;


public class CreaObjectes {

	public static void main (String args[]) throws ParseException{
		
		Session s = HibernateUtil.getSessionFactory().getCurrentSession();
		s.beginTransaction();
		Soci soci = new Soci();
		soci.setNomSoci("Pepito");
		soci.setDomicili("Alcatraz");
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		soci.setDataNaixement(df.parse("03/03/1998"));
		soci.setDataRegistre(df.parse("01/01/2003"));
		s.save(soci);
		s.getTransaction().commit();
		
	}
}
