package es.uji.ei1027.clubesportiu.model;

import java.util.Set;

public class Activitat {

	private String nomActivitat;
	private int vacants;
	private Set<Inscripcio> inscripcions;
	
	public String getNomActivitat() {
		return nomActivitat;
	}
	public void setNomActivitat(String nomActivitat) {
		this.nomActivitat = nomActivitat;
	}
	public int getVacants() {
		return vacants;
	}
	public void setVacants(int vacants) {
		this.vacants = vacants;
	}
	public Set<Inscripcio> getInscripcions() {
		return inscripcions;
	}
	public void setInscripcions(Set<Inscripcio> inscripcions) {
		this.inscripcions = inscripcions;
	}



}
