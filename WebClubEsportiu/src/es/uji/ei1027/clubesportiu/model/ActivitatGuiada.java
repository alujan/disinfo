package es.uji.ei1027.clubesportiu.model;

public class ActivitatGuiada extends Activitat {

	private String[] diaSetmana;
	private int horaInici;
	private Monitor monitor;
	
	public String[] getDiaSetmana() {
		return diaSetmana;
	}
	public void setDiaSetmana(String[] diaSetmana) {
		this.diaSetmana = diaSetmana;
	}
	public int getHoraInici() {
		return horaInici;
	}
	public void setHoraInici(int horaInici) {
		this.horaInici = horaInici;
	}
	public Monitor getMonitor() {
		return monitor;
	}
	public void setMonitor(Monitor monitor) {
		this.monitor = monitor;
	}
	
	
	
	
}
