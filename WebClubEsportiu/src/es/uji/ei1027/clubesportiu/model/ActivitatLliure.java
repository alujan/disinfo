package es.uji.ei1027.clubesportiu.model;

public class ActivitatLliure extends Activitat {

	private String espai;

	public String getEspai() {
		return espai;
	}

	public void setEspai(String espai) {
		this.espai = espai;
	}
	
	
}
