package es.uji.ei1027.clubesportiu.model;

import java.util.Date;

public class Inscripcio {

	private Date dataInici;
	private Date dataFi;
	private Activitat activitat;
	private Soci soci;
	
	public Date getDataInici() {
		return dataInici;
	}
	public void setDataInici(Date dataInici) {
		this.dataInici = dataInici;
	}
	public Date getDataFi() {
		return dataFi;
	}
	public void setDataFi(Date dataFi) {
		this.dataFi = dataFi;
	}
	public Activitat getActivitat() {
		return activitat;
	}
	public void setActivitat(Activitat activitat) {
		this.activitat = activitat;
	}
	public Soci getSoci() {
		return soci;
	}
	public void setSoci(Soci soci) {
		this.soci = soci;
	}
	
	
	
	
}
