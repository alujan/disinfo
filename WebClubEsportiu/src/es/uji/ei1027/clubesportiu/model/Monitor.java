package es.uji.ei1027.clubesportiu.model;

import java.util.Set;


public class Monitor {

	private String nomMonitor;
	private Set<ActivitatGuiada> activitats;
	
	public String getNomMonitor() {
		return nomMonitor;
	}

	public void setNomMonitor(String nomMonitor) {
		this.nomMonitor = nomMonitor;
	}

	public Set<ActivitatGuiada> getActivitats() {
		return activitats;
	}

	public void setActivitats(Set<ActivitatGuiada> activitats) {
		this.activitats = activitats;
	}
	
	
}
