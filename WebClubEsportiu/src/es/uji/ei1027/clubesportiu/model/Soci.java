package es.uji.ei1027.clubesportiu.model;

import java.util.Date;
import java.util.Set;

public class Soci {

	private int numSoci;
	private String nomSoci;
	private String domicili;
	private Date dataNaixement;
	private Date dataRegistre;
	private Set<Inscripcio> inscripcions;
	
	public Set<Inscripcio> getInscripcions() {
		return inscripcions;
	}
	public void setInscripcions(Set<Inscripcio> inscripcions) {
		this.inscripcions = inscripcions;
	}
	public int getNumSoci() {
		return numSoci;
	}
	public void setNumSoci(int numSoci) {
		this.numSoci = numSoci;
	}
	public String getNomSoci() {
		return nomSoci;
	}
	public void setNomSoci(String nomSoci) {
		this.nomSoci = nomSoci;
	}
	public String getDomicili() {
		return domicili;
	}
	public void setDomicili(String domicili) {
		this.domicili = domicili;
	}
	public Date getDataNaixement() {
		return dataNaixement;
	}
	public void setDataNaixement(Date dataNaixement) {
		this.dataNaixement = dataNaixement;
	}
	public Date getDataRegistre() {
		return dataRegistre;
	}
	public void setDataRegistre(Date dataRegistre) {
		this.dataRegistre = dataRegistre;
	}
	
	
}
