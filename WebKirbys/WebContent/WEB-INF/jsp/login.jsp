<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<html>
<c:set var="user" scope="request"
	value='${session.getAttribute( "user")}' />
<head>
<title>Login</title>
</head>
<body>
	<div id="main_container">
		<t:indexheader />

		<div id="login_container">
			<c:choose>
				<c:when test="${user==null}">
					<h2>Access</h2>

					<form:form modelAttribute="user"
						action="${pageContext.request.contextPath}/login.html"
						method="post">
						<table>
							<tr>
								<form:errors path="password" cssClass="error" />
							</tr>
							<tr>
								<td><form:label path="username">User name:</form:label></td>
								<td><form:input path="username" /></td>
								<td><form:errors path="username" cssClass="error" /></td>
							</tr>
							<tr>
								<td><form:label path="password">Password:</form:label></td>
								<td><form:password path="password" /></td>
							</tr>
							<tr>
								<td colspan="2" class="td_submit"><input type="submit"
									value="Submit" /></td>
							</tr>
						</table>
					</form:form>
				</c:when>
				<c:otherwise>
					<h2>You are already logged in</h2>
					<br>
					<a href="${pageContext.request.contextPath}">Home</a>
				</c:otherwise>
			</c:choose>
		</div>
	</div>

</body>
</html>