<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@page import="es.uji.ei1027.kirbys.model.SecondHandCopy"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form"
	prefix="formspring"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Kirby's Comics - Buy</title>
<c:set var="checklist" scope="request"
	value='${session.getAttribute( "checklist")}' />

<c:set var="total" scope="request"
	value='${session.getAttribute("total")}' />

<%!public String getStatus(int status) {
		switch (status) {
		case 1:
			return "Acceptable";
		case 2:
			return "Good";
		case 3:
			return "Very Good";
		case 4:
			return "Like New";
		}
		return "";
	}%>
</head>
<body>
	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div align="center">
			<h2>Buy copies</h2>

			<form method="post"
				action="${pageContext.request.contextPath}/main/buy/purchase.html">
				<label class="error">${message}</label>
				<table>
					<tr>
						<td>Number: <input type="text" autofocus value="copy number"
							name="number" />
						</td>
					</tr>
					<tr>
						<td>Preservation status: <select name="status">
								<option value="4">Like New</option>
								<option value="3">Very Good</option>
								<option value="2">Good</option>
								<option value="1">Acceptable</option>
						</select></td>
					</tr>
					<tr>
						<td>Comic code: <input type="text" value="Comic code"
							name="code" /></td>
					</tr>
					<tr>
						<td><input type="image"
							src="${pageContext.request.contextPath}/res/plus.png" alt="Add"
							title="Add a copy to check out" /></td>
					<tr>
				</table>


			</form>



		</div>
		<div align="center">
			<h1>Check-out list</h1>
			<table>
				<tr>
					<th>Number</th>
					<th>Title</th>
					<th>Price</th>
					<th>Stock</th>
					<th>Preservation status</th>
				</tr>
				<c:forEach items="${checklist}" var="shandcopy">

					<tr>
						<td>${shandcopy.number}</td>
						<td>${shandcopy.comic.title}</td>
						<td>${shandcopy.price}</td>
						<td>${shandcopy.stock}</td>
						<%
							int status = ((SecondHandCopy) pageContext
										.findAttribute("shandcopy")).getPreservationStatus();
						%>
						<td><c:forEach var="i" begin="1" end="<%=status%>">
								<img src="${pageContext.request.contextPath}/res/star.png"
									alt="<%=getStatus(status)%>" title="<%=getStatus(status)%>" />
							</c:forEach></td>

						<td><a
							href="${pageContext.request.contextPath}/main/buy/delete/${shandcopy.publicationId}.html"><img
								src="${pageContext.request.contextPath}/res/iconoEliminar.gif"
								alt="Delte" title="Delte" /></a>
					</tr>
				</c:forEach>
				<tr>
					<th>Total</th>
					<td>${total} euros</td>
				</tr>
				<tr>
					<td><a href="buy/checkout.html"><img
							src="${pageContext.request.contextPath}/res/tick.png"
							alt="Check out" title="Check out" /></a></td>
				</tr>
			</table>

		</div>
	</div>
	<t:help />
</body>
</html>