<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Kirby's Comics - Confirmation</title>

</head>
<body>
	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div align="center">
			<h2>DONE!</h2>
			<label class="success">Your operation has been carried out
				successfuly. Click the button below to continue buying.</label>
			<div id="padding"></div>
			<a href="${pageContext.request.contextPath}/main/buy.html"><button>New
					Purchase</button></a>
		</div>
	</div>
	<t:help />
</body>
</html>