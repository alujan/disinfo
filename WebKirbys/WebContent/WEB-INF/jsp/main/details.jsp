<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@page import="es.uji.ei1027.kirbys.model.SecondHandCopy"%>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<%!public String getStatus(int status) {
		switch (status) {
		case 1:
			return "Acceptable";
		case 2:
			return "Good";
		case 3:
			return "Very Good";
		case 4:
			return "Like New";
		}
		return "";
	}%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Kirby's Comics - Details</title>
</head>
<body>
	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div align="center">
			<h3>Comic summary</h3>
			<table>
				<tr>
					<th>Title</th>
					<th>Genre</th>
					<th>Periodicity</th>
				</tr>
				<tr>
					<td>${comic.title}</td>
					<td>${comic.genre}</td>
					<td>${comic.periodicity}</td>
				</tr>
			</table>
		</div>
		<hr>
		<div id="padding"></div>
		<div align="center">
			<h3>List of NEW copies</h3>
			<table>
				<tr>
					<th>Number</th>
					<th>Price</th>
					<th>Stock</th>
				</tr>
				<c:forEach items="${newcopies}" var="newcopy">
					<tr>
						<td>${newcopy.number}</td>
						<td>${newcopy.price}</td>
						<td>${newcopy.stock}</td>
						<td><a
							href="${pageContext.request.contextPath}/main/sell/sale/${newcopy.publicationId}.html"><img
								src="${pageContext.request.contextPath}/res/arrow.png"
								alt="Sell it" title="Add this copy to check list"></a></td>

					</tr>
				</c:forEach>

			</table>
		</div>
		<div id="padding"></div>
		<div align="center">
			<h3>List of USED copies</h3>
			<table>
				<tr>
					<th>Number</th>
					<th>Price</th>
					<th>Stock</th>
					<th>Preservation status</th>
				</tr>
				<c:forEach items="${shandcopies}" var="shandcopy">

					<tr>
						<td>${shandcopy.number}</td>
						<td>${shandcopy.price}</td>
						<td>${shandcopy.stock}</td>

						<%
							int status = ((SecondHandCopy) pageContext
										.findAttribute("shandcopy")).getPreservationStatus();
						%>
						<td><c:forEach var="i" begin="1" end="<%=status%>">
								<img src="${pageContext.request.contextPath}/res/star.png"
									alt="<%=getStatus(status)%>" title="<%=getStatus(status)%>" />
							</c:forEach></td>
						<td><a
							href="${pageContext.request.contextPath}/main/sell/sale/${shandcopy.publicationId}.html"><img
								src="${pageContext.request.contextPath}/res/arrow.png"
								alt="Sell it" title="Add this copy to check list"></a></td>

					</tr>
				</c:forEach>

			</table>

		</div>
	</div>
	<t:help />
</body>
</html>