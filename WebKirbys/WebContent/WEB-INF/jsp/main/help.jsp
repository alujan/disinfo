<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />

<html>
<head>
<title>Kirby's Comics - Help</title>
</head>
<body>

	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div id="text">
			<h2>FAQ</h2>
			<h3>How do I add a comic?</h3>
			<p>
				1- Go to "Management"<br> 2- If you are not logged in, you will
				be asked your user and password<br> 3- If the login is
				successful you should see the management options.<br> 4- Click
				the "Comics" button and then click in the "+" icon in the page that
				will appear.<br> <br> <i><b>Important Note</b>: You
					have to add the corresponding Publishers and Authors for the comic
					you want to add. That's to say, you can't leave those fields empty.</i><br>
			</p>

			<h3>I cannot login</h3>
			<p>If you have tried several times to log in and you haven't
				succeded, those could be the main causes:</p>
			<ul>
				<li>Your account has expired.Accounts will expire if there is
					not any access in a period of 6 months.</li>
				<li>You have forgotten your password.</li>
				<li>You have misspelled your user, your password or both.</li>
			</ul>
			<p>
				In case you have forgotten your credentials please, send a mail to
				the system administrator to request new ones: <br> <br> <a
					href="mailto:alujan@uji.es?subject=Kirbys%20Assistance&body=I%20have%20forgotten%20my%20password,%20my%20current%20user%20is:[user]">Request
					new password</a><br> <a
					href="mailto:alujan@uji.es?subject=Kirbys%20Assistance&body=I%20have%20forgotten%20my%20user%20and%20password,%20my%20mail%20direction%20is:[mail@myserver.com]">Request
					new account</a><br> <br> <i><b>Note</b>: In the body of
					the mail change the values between [] with the suitable values.</i><br>
			</p>
			<h3>The check out list is empty after adding an item</h3>
			<p>If you don't interact with the site in 15 minutes, your
				session will expire, even if you are not logged in. When the session
				expires the check out list will be reseted and you will have to log
				in again to get management access. When that happens and you add an
				item the page refreshes and then you can see your list empty, it has
				been reseted.</p>
		</div>
	</div>
	<t:help />
</body>
</html>

