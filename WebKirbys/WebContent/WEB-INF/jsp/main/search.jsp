<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Kirby's Comics - Search</title>
</head>

<body>
	<div id="main_container">
		<t:indexheader />

		<div id="option_container">
			<h3>Search Filters</h3>
			<form method="get" action="filter.html">
				<table>
					<tr>
						<td><input type="radio" value="code" name="option" />Code</td>
						<td><input type="radio" value="author" name="option" />Author</td>
					</tr>
					<tr>
						<td><input type="radio" value="title" checked name="option" />
							Title</td>
						<td><input type="radio" value="publisher" name="option" />
							Publisher</td>
						<td><input type="radio" value="genre" name="option" />Genre</td>
					</tr>
					<tr>
						<td><input type="text" value="i.e:Batman" autofocus
							name="filter" /></td>
					</tr>
				</table>
				<input type="submit" value="Search" />
			</form>
		</div>
	</div>

	<div id="padding"></div>
	<div align="center">
		<h1>Found Comics</h1>
		<table>
			<tr>
				<th>Title</th>
				<th>Periodicity</th>
				<th>Genre</th>
			</tr>
			<c:forEach items="${comics}" var="comic">

				<tr>
					<td>${comic.title}</td>
					<td>${comic.periodicity}</td>
					<td>${comic.genre}</td>
					<td><a href="details/${comic.code}.html"><img
							src="${pageContext.request.contextPath}/res/details.png"
							alt="Details" title="See Details" /></a>
				</tr>
			</c:forEach>
		</table>

	</div>
	<t:help />
</body>
</html>