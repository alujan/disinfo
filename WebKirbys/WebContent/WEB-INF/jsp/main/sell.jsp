<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Kirby's Comics - Sell</title>
<c:set var="checklist" scope="request"
	value='${session.getAttribute( "checklist")}' />
</head>
<body>
<body>
	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div align="center">
			<h2>Sell Comics</h2>

			<form method="post"
				action="${pageContext.request.contextPath}/main/sell/sale.html">
				<label class="error">${message}</label>
				<table>
					<tr>
						<td>Number: <input type="text" autofocus value="copy number"
							name="number" />
						</td>
					</tr>

					<tr>
						<td>Used<input type="checkbox" name="used" value="true" /></td>
						<td>Preservation status: <select name="status">
								<option value="4">Like New</option>
								<option value="3">Very Good</option>
								<option value="2">Good</option>
								<option value="1">Acceptable</option>
						</select></td>
					</tr>
					<tr>
						<td>Comic code: <input type="text" value="Comic code"
							name="code" /></td>
					</tr>
					<tr>
						<td><input type="image"
							src="${pageContext.request.contextPath}/res/plus.png" alt="Add"
							title="Add a copy to check out" /></td>
					<tr>
				</table>


			</form>

		</div>

	</div>

	<div id="padding"></div>
	<div align="center">
		<h1>Check-out list</h1>
		<table>
			<tr>
				<th>Number</th>
				<th>Title</th>
				<th>Price</th>
				<th>Stock</th>
			</tr>
			<c:forEach items="${checklist}" var="copy">

				<tr>
					<td>${copy.number}</td>
					<td>${copy.comic.title}</td>
					<td>${copy.price}</td>
					<td>${copy.stock}</td>


					<td><a
						href="${pageContext.request.contextPath}/main/sell/delete/${copy.publicationId}.html"><img
							src="${pageContext.request.contextPath}/res/iconoEliminar.gif"
							alt="Delte" title="Delte" /></a>
				</tr>
			</c:forEach>
			<tr>
				<th>Total</th>
				<td>${total} euros</td>
			</tr>
			<tr>
				<td><a
					href="${pageContext.request.contextPath}/main/sell/checkout.html"><img
						src="${pageContext.request.contextPath}/res/tick.png"
						alt="Check out" title="Check out" /></a></td>
			</tr>
		</table>

	</div>
	<t:help />

</body>
</html>