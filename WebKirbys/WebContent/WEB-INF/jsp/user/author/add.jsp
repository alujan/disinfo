<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Kirby's Comics</title>


</head>

<body>

	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>

		<div align="center">
			<h2>Add new author</h2>
			<form:form method="post" modelAttribute="author">
				<table>
					<tr>
						<td><form:label path="name">Name</form:label></td>
						<td><form:input path="name" /></td>
					</tr>
					<tr>
						<td><form:label path="pseudonym">Pseudonym</form:label></td>
						<td><form:input path="pseudonym" /></td>
					</tr>
					<tr>
						<td><input type="submit" value="Add Author" /></td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
	<t:help />
</body>
</html>