<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />


<html>
<head>
<title>Manage comics</title>
</head>

<body>

	<div id="main_container">
		<t:indexheader />

		<div id="option_container">
			<h2>Authors</h2>
			<table>
				<tr>
					<th>Name</th>
					<th>Pseudonym</th>
					<th>Id</th>
				</tr>
				<c:forEach items="${authors}" var="author">
					<tr>
						<td>${author.name}</td>
						<td>${author.pseudonym}</td>
						<td>${author.authorId}</td>
						<td><a href="update/${author.authorId}.html"><img
								src="${pageContext.request.contextPath}/res/iconoEditar.gif"></a>
						<td><a href="delete/${author.authorId}.html"><img
								src="${pageContext.request.contextPath}/res/iconoEliminar.gif"></a>
					</tr>
				</c:forEach>
				<tr>
					<td><a href="add.html"><img
							src="${pageContext.request.contextPath}/res/plus.png"></a></td>
				</tr>
			</table>
		</div>

	</div>
	<t:help />
</body>
</html>