<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Kirby's Comics</title>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />

</head>

<body>

	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div align="center">
			<h2>Add new comic</h2>
			<form:form method="post" modelAttribute="comic">
				<form:errors path="title" class="error"></form:errors>
				<form:errors path="genre" class="error"></form:errors>
				<form:errors path="periodicity" class="error"></form:errors>
				<form:errors path="publisher" class="error"></form:errors>
				<form:errors path="authors" class="error"></form:errors>
				<table>
					<tr>
						<td><form:label path="title">Title</form:label></td>
						<td><form:input path="title" /></td>
					</tr>

					<tr>
						<td><form:label path="genre">Genre</form:label></td>
						<td><form:input path="genre" /></td>
					</tr>
					<tr>
						<td><form:label path="periodicity">Periodicity</form:label></td>
						<td><form:input path="periodicity" /></td>
					</tr>
					<tr>
						<td><form:label path="publisher">Publisher</form:label></td>
						<td><form:input path="publisher" value="NIF" /></td>
					</tr>
					<tr>
						<td><form:label path="authors">Authors</form:label></td>
						<td><form:select path="authors" multiple="true">
								<form:options items="${authorsList}" itemValue="authorId"
									itemLabel="name" />
							</form:select></td>
					</tr>
					<tr>
						<td><input type="submit" value="Add Comic" /></td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
	<t:help />
</body>
</html>