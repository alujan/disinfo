<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="es.uji.ei1027.kirbys.model.Comic"%>
<%@page import="es.uji.ei1027.kirbys.model.Author"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />


<html>
<head>
<title>Kirby's Comics - Management: Comics List</title>
</head>
<body>

	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div align="center">
			<h2>Comics</h2>
			<table>
				<tr>
					<th>Code</th>
					<th>Title</th>
					<th>Authors</th>
					<th>Genre</th>
					<th>Start of publishing</th>
					<th>End of publishing</th>
					<th></th>
					<th></th>
				</tr>
				<c:forEach items="${comics}" var="comic">
					<tr>
						<td>${comic.code}</td>
						<td>${comic.title}</td>
						<td><c:forEach items="${comic.authors}" var="author">
						${author.name} 
						</c:forEach></td>
						<td>${comic.genre}</td>
						<td>${comic.startOfPublishing}</td>
						<td>${comic.endOfPublishing}</td>
						<td><a href="update/${comic.code}.html"><img
								src="${pageContext.request.contextPath}/res/iconoEditar.gif" /></a>
						<td><a href="delete/${comic.code}.html"><img
								src="${pageContext.request.contextPath}/res/iconoEliminar.gif" /></a>
					</tr>
				</c:forEach>
				<tr>
					<td><a href="add.html"><img
							src="${pageContext.request.contextPath}/res/plus.png" /></a></td>
				</tr>
			</table>
		</div>



	</div>
	<t:help />
</body>
</html>