<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Kirby's Comics</title>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />

</head>

<body>

	<div id="main_container">
		<t:indexheader />

		<div id="padding"></div>
		<div align="center">
			<h2>Modify comic</h2>
			<form:form method="post" modelAttribute="comic">
				<table>
					<tr>
						<td><form:label path="title">Title</form:label></td>
						<td><form:input path="title" /></td>
					</tr>
					<tr>
						<td><form:label path="genre">Genre</form:label></td>
						<td><form:input path="genre" /></td>
					</tr>
					<tr>
						<td><form:label path="periodicity">Periodicity</form:label></td>
						<td><form:input path="periodicity" /></td>
					</tr>
					<tr>
						<form:hidden path="code" />
						<form:hidden path="startOfPublishing" />
						<form:hidden path="publisher" />
						<td><input type="submit" value="Modify" /></td>
					</tr>

				</table>
			</form:form>
		</div>
	</div>
	<t:help />
</body>
</html>