<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="US-ASCII"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kirby's Comics - Management: Add New Copy</title>
</head>
<body>
	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>

		<div align="center">
			<h2>New Copy</h2>
			<form:form method="post" modelAttribute="newcopy">
				<table>
					<form:errors path="number" class="error"></form:errors>
					<form:errors path="comic" class="error"></form:errors>
					<tr>
						<td><form:label path="number">Number</form:label></td>
						<td><form:input path="number" /></td>
					</tr>
					<tr>
						<td><form:label path="price">Price</form:label></td>
						<td><form:input path="price" /></td>
					</tr>
					<tr>
						<td><form:label path="stock">Stock</form:label></td>
						<td><form:input path="stock" /></td>
					</tr>
					<tr>
						<td><form:label path="comic">Comic</form:label></td>
						<td><form:select path="comic" multiple="false">
								<form:options items="${comics}" itemValue="code"
									itemLabel="title" />
							</form:select></td>
					</tr>
					<tr>
						<td><input type="submit" value="Add Copy" /></td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
	<t:help />
</body>
</html>