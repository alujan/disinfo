<%@page import="es.uji.ei1027.kirbys.model.SecondHandCopy"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />

<%!public String getStatus(int status) {
		switch (status) {
		case 1:
			return "Acceptable";
		case 2:
			return "Good";
		case 3:
			return "Very Good";
		case 4:
			return "Like New";
		}
		return "";
	}%>

<html>
<head>
<title>Kirby's Comics - Management: Publications</title>
</head>
<body>
	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div align="center">
			<h2>New Copies list</h2>
			<table>
				<tr>
					<th>Number</th>
					<th>Title</th>
					<th>Price</th>
					<th>Stock</th>
					<th>Income date</th>
				</tr>
				<c:forEach items="${newcopies}" var="newcopy">
					<tr>
						<td>${newcopy.number}</td>
						<td>${newcopy.comic.title}</td>
						<td>${newcopy.price}</td>
						<td>${newcopy.stock}</td>
						<td>${newcopy.incomeDate}</td>
						<td><a
							href="publication/updatenew/${newcopy.publicationId}.html"><img
								src="${pageContext.request.contextPath}/res/iconoEditar.gif"
								alt="Modify" title="Modify"></a></td>
						<td><a
							href="publication/deletenew/${newcopy.publicationId}.html"><img
								src="${pageContext.request.contextPath}/res/iconoEliminar.gif"
								alt="Delte" title="Delte"></a></td>
					</tr>
				</c:forEach>
				<tr>
					<td><a href="publication/addnew.html"><img
							src="${pageContext.request.contextPath}/res/plus.png"
							alt="Add a new copy" title="Add a new copy" /></a></td>
				</tr>
			</table>
		</div>
		<div id="padding"></div>
		<div align="center">
			<h1>Used Copies list</h1>
			<table>
				<tr>
					<th>Number</th>
					<th>Title</th>
					<th>Price</th>
					<th>Stock</th>
					<th>Preservation status</th>
				</tr>
				<c:forEach items="${shandcopies}" var="shandcopy">

					<tr>
						<td>${shandcopy.number}</td>
						<td>${shandcopy.comic.title}</td>
						<td>${shandcopy.price}</td>
						<td>${shandcopy.stock}</td>
						<%
							int status = ((SecondHandCopy) pageContext
										.findAttribute("shandcopy")).getPreservationStatus();
						%>
						<td><c:forEach var="i" begin="1" end="<%=status%>">
								<img src="${pageContext.request.contextPath}/res/star.png"
									alt="<%=getStatus(status)%>" title="<%=getStatus(status)%>" />
							</c:forEach></td>

						<td><a
							href="publication/updateshand/${shandcopy.publicationId}.html"><img
								src="${pageContext.request.contextPath}/res/iconoEditar.gif"
								alt="Modify" title="Modify" /></a>
						<td><a
							href="publication/deleteshand/${shandcopy.publicationId}.html"><img
								src="${pageContext.request.contextPath}/res/iconoEliminar.gif"
								alt="Delte" title="Delte" /></a>
					</tr>
				</c:forEach>
				<tr>
					<td><a href="publication/addshand.html"><img
							src="${pageContext.request.contextPath}/res/plus.png"
							alt="Add a second-hand copy" title="Add a second-hand copy" /></a></td>
				</tr>
			</table>

		</div>
	</div>
	<t:help />
</body>
</html>