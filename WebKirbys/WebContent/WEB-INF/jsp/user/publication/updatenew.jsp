<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Kirby's Comics - Management: Modify New Copy</title>
</head>
<body>
	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>

		<div align="center">
			<h2>Modify New Copy</h2>
			<form:form method="post" modelAttribute="newcopy">

				<table>
					<form:errors path="comic" />
					<form:errors path="price" />
					<form:errors path="stock" />

					<tr>
						<td><form:label path="price">Price</form:label></td>
						<td><form:input path="price" /></td>
					</tr>
					<tr>
						<td><form:label path="stock">Stock</form:label></td>
						<td><form:input path="stock" /></td>
					</tr>
					<tr>
						<form:hidden path="publicationId" />
						<form:hidden path="number" />
						<form:hidden path="incomeDate" />
						<form:hidden path="comic" />
						<td><input type="submit" value="Modify" /></td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
	<t:help />
</body>
</html>