<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Kirby's Comics - Management: Modify SecondHand Copy</title>
</head>
<body>
	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>

		<div align="center">
			<h2>Modify Second-Hand Copy</h2>
			<form:form method="post" modelAttribute="shandcopy">
				<table>
					<tr>
						<td><form:label path="price">Price</form:label></td>
						<td><form:input path="price" /></td>
					</tr>
					<tr>
						<td><form:label path="stock">Stock</form:label></td>
						<td><form:input path="stock" /></td>
					</tr>
					<%-- 					<tr> --%>
					<%-- 						<td><form:label path="preservationStatus">Preservation Status</form:label></td> --%>
					<%-- 						<td><form:select path="preservationStatus"> --%>
					<%-- 								<form:option value="4" label="Like New" /> --%>
					<%-- 								<form:option value="3" label="Very Good" /> --%>
					<%-- 								<form:option value="2" label="Good" /> --%>
					<%-- 								<form:option value="1" label="Acceptable" /> --%>
					<%-- 							</form:select></td> --%>
					<%-- 					</tr> --%>
					<tr>
						<form:hidden path="number" />
						<form:hidden path="publicationId" />
						<form:hidden path="comic" />
						<form:hidden path="preservationStatus" />
						<td><input type="submit" value="Modify" /></td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
	<t:help />
</body>
</html>