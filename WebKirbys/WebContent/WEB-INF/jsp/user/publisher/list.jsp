<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<html>
<head>
<title>Kirby's Comics - Management: Publishers</title>
</head>
<body>
	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div align="center">
			<h1>Publishers list</h1>
			<label class="error">${message}</label>
			<table>
				<tr>
					<th>NIF</th>
					<th>Address</th>
					<th>Telephone</th>
				</tr>
				<c:forEach items="${publishers}" var="publisher">
					<tr>
						<td>${publisher.nif}</td>
						<td>${publisher.address}</td>
						<td>${publisher.telephone}</td>
						<td><a
							href="${pageContext.request.contextPath}/user/publisher/update/${publisher.publisherId}.html"><img
								src="${pageContext.request.contextPath}/res/iconoEditar.gif" /></a>
						<td><a
							href="${pageContext.request.contextPath}/user/publisher/delete/${publisher.publisherId}.html"><img
								src="${pageContext.request.contextPath}/res/iconoEliminar.gif" /></a>
					</tr>
				</c:forEach>
				<tr>
					<td><a
						href="${pageContext.request.contextPath}/user/publisher/add.html"
						class="td_submit"><img
							src="${pageContext.request.contextPath}/res/plus.png" /></a></td>
				</tr>
			</table>
		</div>
	</div>
	<t:help />
</body>
</html>