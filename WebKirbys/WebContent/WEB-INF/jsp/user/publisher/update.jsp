<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Kirby's Comics - Management: Update Publisher</title>
</head>
<body>
	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div align="center">
			<h2>Modify Publisher</h2>
			<form:form method="post" modelAttribute="publisher">
				<table>
					<tr>
						<td><form:label path="nif">NIF</form:label></td>
						<td><form:input path="nif" /></td>
					</tr>
					<tr>
						<td><form:label path="address">Address</form:label></td>
						<td><form:input path="address" /></td>
					</tr>
					<tr>
						<td><form:label path="telephone">Telephone number</form:label></td>
						<td><form:input path="telephone" /></td>
					</tr>
					<tr>
						<form:hidden path="publisherId" />
						<td><input type="submit" value="Modify" /></td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>
	<t:help />
</body>
</html>