<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />


<html>
<head>
<title>Kirby's Comics - Management: Purchases Search</title>
</head>
<body>

	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div align="center">
			<h2>Search Purchase</h2>
			<form method="get"
				action="${pageContext.request.contextPath}/user/purchase/detail.html">
				<table>
					<tr>
						<td>Purchase code: <input type="text" value="i.e:745439"
							autofocus name="filter" /></td>
					</tr>
				</table>
				<input type="submit" value="See details" />
			</form>
		</div>
	</div>
	<t:help />
</body>
</html>