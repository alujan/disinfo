<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="es.uji.ei1027.kirbys.model.SecondHandCopy"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<html>
<head>
<title>Kirby's Comics - Management: Sale Details</title>
</head>
<body>
	<div id="main_container">
		<t:indexheader />
		<div id="padding"></div>
		<div align="center">
			<h2>Sale details</h2>
			<table>
				<tr>
					<th>Code</th>
					<td>${sale.code}</td>
				</tr>
				<tr>
					<th>Date</th>
					<td>${sale.date}</td>
				</tr>
			</table>

			<h3>Copies sold</h3>
			<table>
				<tr>
					<th>Number</th>
					<th>Title</th>
					<th>Price</th>
				</tr>
				<c:forEach items="${copies}" var="copy">
					<tr>
						<td>${copy.number}</td>
						<td>${copy.comic.title}</td>
						<td>${copy.price}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<t:help />
</body>
</html>