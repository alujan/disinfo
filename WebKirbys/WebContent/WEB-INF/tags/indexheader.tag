<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />

<c:set var="user" scope="request"
	value='${session.getAttribute( "user")}' />

<div align="right">
	<t:logininfo />
</div>
<div id="padding"></div>
<div align="right">
	<t:search />
</div>

<div id="cabecera">
	<a href="${pageContext.request.contextPath}"><img
		src="${pageContext.request.contextPath}/res/headerKirby.png" /></a>
</div>
<div>
	<c:choose>
		<c:when test="${user==null}">
			<div id="infobar_public"></div>
		</c:when>
		<c:otherwise>
			<div id="infobar_admin"></div>
		</c:otherwise>
	</c:choose>
	<div id="menu_option">
		<a href="${pageContext.request.contextPath}/main/buy.html">Buy a
			comic</a>
	</div>
	<div id="menu_option">
		<a href="${pageContext.request.contextPath}/main/sell.html">Sell a
			comic</a>
	</div>
	<div id="menu_option">
		<a href="${pageContext.request.contextPath}/user/management.html">Management</a>
	</div>


</div>


