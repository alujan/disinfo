<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="user" scope="request"
	value='${session.getAttribute( "user")}' />

<c:choose>
	<c:when test="${user==null}">
		<a href="${pageContext.request.contextPath}/login.html">Login</a>
	</c:when>
	<c:otherwise>
		<table>
			<tr>
				<td>Welcome ${user.username}</td>
				<td><a href="${pageContext.request.contextPath}/logout.html">Logout</a></td>
			</tr>
		</table>
	</c:otherwise>
</c:choose>

