<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />

<div id="management_container">
	<h2>Management</h2>
	<table>
		<tr>
			<td><a href="comic/list.html"><img
					src="${pageContext.request.contextPath}/res/Buttons/ComicsButton.png" /></a></td>
		</tr>
		<tr>
			<td><a href="author/list.html"><img
					src="${pageContext.request.contextPath}/res/Buttons/AuthorsButton.png" /></a></td>
		</tr>
		<tr>
			<td><a href="publication.html"><img
					src="${pageContext.request.contextPath}/res/Buttons/PubButton.png" /></a></td>
		</tr>
		<tr>
			<td><a href="publisher.html"><img
					src="${pageContext.request.contextPath}/res/Buttons/PubsherButton.png" /></a></td>
		</tr>
		<tr>
			<td><a href="purchase.html"><img
					src="${pageContext.request.contextPath}/res/Buttons/PurchasesButton.png" /></a></td>
		</tr>
		<tr>
			<td><a href="sale.html"><img
					src="${pageContext.request.contextPath}/res/Buttons/SalesButton.png" /></a></td>
		</tr>

	</table>
</div>

