<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />

<html>
<head>
<title>Kirby's Comics</title>
</head>
<body>

	<div id="main_container">
		<t:indexheader />
		<t:recentadded />
	</div>
	<t:help />
</body>
</html>

