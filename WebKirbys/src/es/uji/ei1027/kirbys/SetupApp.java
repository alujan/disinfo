package es.uji.ei1027.kirbys;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import es.uji.ei1027.kirbys.model.Author;
import es.uji.ei1027.kirbys.model.Comic;
import es.uji.ei1027.kirbys.model.NewCopy;
import es.uji.ei1027.kirbys.model.Publisher;
import es.uji.ei1027.kirbys.model.SecondHandCopy;

public class SetupApp implements ServletContextListener {

	private static final SessionFactory sessionFactory = buildSessionFactory();
	private static final SimpleDateFormat df = new SimpleDateFormat(
			"dd/MM/yyyy");

	private List<Author> authors = new LinkedList<Author>();
	private List<Publisher> publishers = new LinkedList<Publisher>();
	private List<Comic> comics = new LinkedList<Comic>();

	private static SessionFactory buildSessionFactory() {

		try {
			// Create the SessionFactory from hibernate.cfg.xml
			Configuration configuration = new Configuration();
			configuration.configure();
			ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
					.applySettings(configuration.getProperties())
					.buildServiceRegistry();
			return configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err
					.println("Error en la creaci� de la SessionFactory." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {

		System.out.println("Initializing application at "
				+ sce.getServletContext().getContextPath()
				+ ". Wait, please... ");

		Session s = sessionFactory.getCurrentSession();
		s.beginTransaction();

		addAuthors(s);
		addPublishers(s);
		addComics(s);
		addPublications(s);
		s.getTransaction().commit();

		System.out.println("APP INIALIZED SUCCESSFULY");
	}

	private void addPublications(Session s) {

		Comic comic;
		/* Add Publications */
		NewCopy newcopy = new NewCopy();
		newcopy.setComic(comics.get(0));
		newcopy.setNumber(1);
		newcopy.setPrice(11.23);
		try {
			newcopy.setIncomeDate(df.parse("23/6/2012"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		newcopy.setStock(13);
		s.save(newcopy);
		comic = comics.get(0);
		comic.getPublications().add(newcopy);
		s.update(comic);

		newcopy = new NewCopy();
		newcopy.setNumber(13);
		newcopy.setComic(comics.get(1));
		newcopy.setPrice(12.66);
		try {
			newcopy.setIncomeDate(df.parse("29/8/2011"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		newcopy.setStock(2);
		s.save(newcopy);
		comic = comics.get(1);
		comic.getPublications().add(newcopy);
		s.update(comic);

		newcopy = new NewCopy();
		newcopy.setNumber(1);
		newcopy.setComic(comics.get(2));
		newcopy.setPrice(13.99);
		try {
			newcopy.setIncomeDate(df.parse("1/3/2011"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		newcopy.setStock(7);
		s.save(newcopy);
		comic = comics.get(2);
		comic.getPublications().add(newcopy);
		s.update(comic);

		SecondHandCopy shandcopy = new SecondHandCopy();
		shandcopy.setNumber(1);
		shandcopy.setPreservationStatus(2);
		shandcopy.setPrice(7.6);
		shandcopy.setComic(comics.get(2));
		shandcopy.setStock(4);
		s.save(shandcopy);
		comic = comics.get(2);
		comic.getPublications().add(shandcopy);
		s.update(comic);

		shandcopy = new SecondHandCopy();
		shandcopy.setNumber(1);
		shandcopy.setPreservationStatus(4);
		shandcopy.setPrice(6.33);
		shandcopy.setComic(comics.get(0));
		shandcopy.setStock(2);
		s.save(shandcopy);
		comic = comics.get(0);
		comic.getPublications().add(shandcopy);
		s.update(comic);

		shandcopy = new SecondHandCopy();
		shandcopy.setNumber(1);
		shandcopy.setPreservationStatus(1);
		shandcopy.setPrice(3.33);
		shandcopy.setComic(comics.get(0));
		shandcopy.setStock(3);
		s.save(shandcopy);
		comic = comics.get(0);
		comic.getPublications().add(shandcopy);
		s.update(comic);
	}

	private void addComics(Session s) {

		Set<Author> authors1 = new HashSet<Author>();
		authors1.add(authors.get(0));
		authors1.add(authors.get(1));
		Set<Author> authors2 = new HashSet<Author>();
		authors2.add(authors.get(0));
		authors2.add(authors.get(2));
		Set<Author> authors3 = new HashSet<Author>();
		authors3.add(authors.get(2));

		/* Add Comics */
		Comic comic = new Comic();
		comic.setTitle("Sakura, Card captor");
		comic.setPeriodicity(14);
		comic.setGenre("fantasy,magic");

		try {
			comic.setStartOfPublishing(df.parse("13/07/1999"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		comic.setAuthors(authors1);
		comic.setPublisher(publishers.get(1));
		s.save(comic);
		comics.add(comic);

		comic = new Comic();
		comic.setTitle("X-MEN");
		comic.setPeriodicity(7);
		comic.setGenre("fantasy,heros,mutant");

		try {
			comic.setStartOfPublishing(df.parse("21/02/1906"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		comic.setAuthors(authors2);
		comic.setPublisher(publishers.get(1));
		s.save(comic);
		comics.add(comic);

		comic = new Comic();
		comic.setTitle("Full Metal Alchemist");
		comic.setPeriodicity(21);
		comic.setGenre("Magic,alchemy,fantasy,heros");

		try {
			comic.setStartOfPublishing(df.parse("7/1/2000"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		comic.setAuthors(authors3);
		comic.setPublisher(publishers.get(2));
		s.save(comic);
		comics.add(comic);

	}

	private void addPublishers(Session s) {

		/* Add Publishers */
		Publisher publisher = new Publisher();

		publisher.setAddress("La calle del bacalao");
		publisher.setTelephone(964532871);
		publisher.setNif("20N");
		s.save(publisher);
		publishers.add(publisher);
		publisher = new Publisher();
		publisher.setAddress("La puerta de Alcalá");
		publisher.setTelephone(964992211);
		publisher.setNif("13M");
		s.save(publisher);
		publishers.add(publisher);
		publisher = new Publisher();
		publisher.setAddress("Manhattan");
		publisher.setTelephone(566712);
		publisher.setNif("7XE");
		s.save(publisher);
		publishers.add(publisher);

	}

	private void addAuthors(Session s) {

		/* Add authors */
		Author author = new Author();
		author.setName("Jacob Kurtzberg");
		author.setPseudonym("Jack Kirby");
		s.save(author);
		authors.add(author);
		author = new Author();
		author.setName("James P. Starlin");
		author.setPseudonym("Jim Starlin");
		s.save(author);
		authors.add(author);
		author = new Author();
		author.setName("Mark Millar");
		author.setPseudonym("");
		s.save(author);
		authors.add(author);

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {

		System.out.println("Finalizing application "
				+ sce.getServletContext().getContextPath());

	}

}