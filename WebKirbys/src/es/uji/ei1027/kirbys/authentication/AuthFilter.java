package es.uji.ei1027.kirbys.authentication;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthFilter implements Filter {

	@Override
	public void destroy() {
		// TODO

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;

		// Checks the user authentication status
		HttpSession session = request.getSession();
		boolean isAuthenticated = session.getAttribute("user") != null;
		if (isAuthenticated){
			chain.doFilter(request, response);
		}else {

			String contextPath = request.getContextPath();
			String requestURI = request.getRequestURI();

			if (requestURI.startsWith(contextPath + "/user")) {

				String nextView = requestURI.substring(contextPath.length());
				session.setAttribute("nextView", nextView);
				response.sendRedirect(request.getContextPath() + "/login.html");
			} else {
				chain.doFilter(request, response);
			}
		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO

	}

}
