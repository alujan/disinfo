package es.uji.ei1027.kirbys.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.uji.ei1027.kirbys.dao.AuthorDao;
import es.uji.ei1027.kirbys.model.Author;
import es.uji.ei1027.kirbys.model.Comic;

@Controller
@RequestMapping("/user/author")
public class AuthorController {

	@Autowired
	private AuthorDao authorDao;

	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView listAuthors() {
		return new ModelAndView("user/author/list", "authors",
				authorDao.listAuthors());
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView addAuthor() {
		return new ModelAndView("user/author/add", "author", new Author());
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String processAuthorAdd(@ModelAttribute("author") Author author,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors())
			return "user/author/add";

		author.setComics(new HashSet<Comic>());
		authorDao.addAuthor(author);
		return "redirect:list.html";
	}

	@RequestMapping(value = "/update/{authorId}", method = RequestMethod.GET)
	public ModelAndView updateAuthor(@PathVariable int authorId) {
		return new ModelAndView("user/author/update", "author",
				authorDao.getAuthorById(authorId));
	}

	@RequestMapping(value = "/update/{authorId}", method = RequestMethod.POST)
	public String processAuthorUpdate(@PathVariable int authorId,
			@ModelAttribute("author") Author author, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			return "user/author/update";
		}
		authorDao.updateAuthor(author);
		return "redirect:../list.html";
	}

	
	@RequestMapping(value = "/delete/{authorId}", method = RequestMethod.GET)
	public String deleteAuthor(@PathVariable int authorId) {
		authorDao.deleteAuthor(authorId);
		return "redirect:../list.html";
	}

}
