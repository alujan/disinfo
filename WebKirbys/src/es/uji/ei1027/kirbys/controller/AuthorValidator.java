package es.uji.ei1027.kirbys.controller;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import es.uji.ei1027.kirbys.model.Author;

public class AuthorValidator implements Validator {

	@Override
	public boolean supports(Class<?> cls) {
		return Author.class.equals(cls);
	}

	@Override
	public void validate(Object obj, Errors errors) {

		Author author = (Author) obj;
		if (author.getName().trim().equals(""))
			errors.rejectValue("name", "required",
					"Must enter a valid name ( not empty)");

	}

}
