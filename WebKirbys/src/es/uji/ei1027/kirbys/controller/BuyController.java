package es.uji.ei1027.kirbys.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.uji.ei1027.kirbys.dao.ComicDao;
import es.uji.ei1027.kirbys.dao.NewCopyDao;
import es.uji.ei1027.kirbys.dao.PurchaseDao;
import es.uji.ei1027.kirbys.dao.SecondHandCopyDao;
import es.uji.ei1027.kirbys.model.Comic;
import es.uji.ei1027.kirbys.model.Purchase;
import es.uji.ei1027.kirbys.model.SecondHandCopy;

@Controller
@RequestMapping("/main/buy")
public class BuyController {

	@Autowired
	private SecondHandCopyDao sHandDao;

	@Autowired
	private PurchaseDao purchaseDao;

	@Autowired
	private NewCopyDao newDao;

	@Autowired
	private ComicDao comicDao;

	public void setComicDao(ComicDao comicDao) {
		this.comicDao = comicDao;
	}

	public void setNewDao(NewCopyDao newDao) {
		this.newDao = newDao;
	}

	public void setSecondHandCopyDao(SecondHandCopyDao sHandDao) {
		this.sHandDao = sHandDao;
	}

	public void setPurchaseDao(SecondHandCopyDao sHandDao) {
		this.sHandDao = sHandDao;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String listComics(HttpSession session) {
		if (session.getAttribute("checklist") == null) {
			session.setAttribute("checklist", new LinkedList<SecondHandCopy>());
			session.setAttribute("total", new Double(0.0));
		}
		return "main/buy";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/purchase", method = RequestMethod.POST)
	public String addPurchase(HttpServletRequest req, HttpSession session,
			ModelMap model) {

		SecondHandCopy toPurchase;
		int number = Integer.parseInt(req.getParameter("number"));
		int status = Integer.parseInt(req.getParameter("status"));
		int code = Integer.parseInt(req.getParameter("code"));
		double total = (Double) session.getAttribute("total");

		List<SecondHandCopy> checklist = (List<SecondHandCopy>) session
				.getAttribute("checklist");

		if (!newDao.exists(number, code)) {
			model.addAttribute("message",
					"The number of the copy does not exist in the system");
			return "main/buy";
		} else if (sHandDao.exists(number, code, status)) {
			toPurchase = sHandDao.getSecondHandCopyByParams(number, code,
					status);
		} else {
			toPurchase = new SecondHandCopy();
			toPurchase.setNumber(number);
			toPurchase.setPreservationStatus(status);
			Comic comic = comicDao.getComicById(code);
			toPurchase.setComic(comic);
			double price = (newDao.getNewCopyByParams(number, code).getPrice()
					* status * 1. / 4) - 1;
			toPurchase.setPrice(Math.rint(price * 100) / 100);
			toPurchase.setStock(0);
			sHandDao.addSecondHandCopy(toPurchase);
			comicDao.addPublicationToComic(toPurchase);

		}

		total += toPurchase.getPrice();
		total = Math.rint(total * 100) / 100;
		checklist.add(toPurchase);
		session.setAttribute("checklist", checklist);
		session.setAttribute("total", total);
		return "redirect:../buy.html";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/delete/{publicationId}", method = RequestMethod.GET)
	public String delete(@PathVariable int publicationId, HttpSession session) {

		List<SecondHandCopy> checklist = (List<SecondHandCopy>) session
				.getAttribute("checklist");
		double total = (Double) session.getAttribute("total");
		SecondHandCopy toDelete = sHandDao.getSecondHandCopyById(publicationId);
		total -= toDelete.getPrice();
		total = Math.rint(total * 100) / 100;
		checklist = this.delteCopy(checklist, toDelete);
		session.setAttribute("checklist", checklist);
		session.setAttribute("total", total);
		return "redirect:../../buy.html";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/checkout", method = RequestMethod.GET)
	public String checkOut(HttpSession session) {

		List<SecondHandCopy> checklist = (List<SecondHandCopy>) session
				.getAttribute("checklist");
		double total = (Double) session.getAttribute("total");

		Purchase purchase = purchaseDao.createPurchase();
		List<SecondHandCopy> inpurchase = new LinkedList<SecondHandCopy>();

		for (SecondHandCopy shandcopy : checklist) {
			inpurchase.add(shandcopy);
			sHandDao.doPurchase(shandcopy, purchase);
		}

		purchase.setsHandCopies(inpurchase);
		purchase.setTotal(total);
		purchaseDao.updatePurchase(purchase);
		session.setAttribute("checklist", new LinkedList<SecondHandCopy>());
		session.setAttribute("total", new Double(0.0));
		return "main/buyconfirm";
	}

	private List<SecondHandCopy> delteCopy(List<SecondHandCopy> list,
			SecondHandCopy toDelete) {

		for (SecondHandCopy copy : list) {
			if (copy.getPublicationId() == toDelete.getPublicationId()) {
				toDelete = copy;
				break;
			}
		}
		list.remove(toDelete);
		return list;
	}
}
