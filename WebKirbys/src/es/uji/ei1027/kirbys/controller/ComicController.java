package es.uji.ei1027.kirbys.controller;

import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.uji.ei1027.kirbys.dao.AuthorDao;
import es.uji.ei1027.kirbys.dao.ComicDao;
import es.uji.ei1027.kirbys.dao.PublicationDao;
import es.uji.ei1027.kirbys.dao.PublisherDao;
import es.uji.ei1027.kirbys.model.Author;
import es.uji.ei1027.kirbys.model.Comic;
import es.uji.ei1027.kirbys.model.Publisher;

@Controller
@RequestMapping("/user/comic")
public class ComicController {

	@Autowired
	private ComicDao comicDao;

	@Autowired
	private AuthorDao authorDao;

	@Autowired
	private PublisherDao publisherDao;

	@Autowired
	private PublicationDao publicationDao;

	public void setPublicationDao(PublicationDao publicationDao) {
		this.publicationDao = publicationDao;
	}

	public void setComicDao(ComicDao comicDao) {
		this.comicDao = comicDao;
	}

	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	public void setPublisherDao(PublisherDao publisherDao) {
		this.publisherDao = publisherDao;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));

		binder.registerCustomEditor(Set.class, "authors",
				new CustomCollectionEditor(Set.class) {

					@Override
					protected Object convertElement(Object element) {
						Integer authorId = Integer.parseInt(((String) element));
						return authorDao.getAuthorById(authorId);
					}
				});

	
		binder.registerCustomEditor(Publisher.class, "publisher",
				new PropertyEditorSupport() {

					public void setAsText(String incomming) {
						Publisher publisher = publisherDao
								.getPublisherByNIF(incomming);
						setValue(publisher);
					}

					public String getAsText() {
						Publisher pub = (Publisher) getValue();
						if (pub == null)
							return null;
						return pub.getNif();
					}

				});
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView listComics() {
		return new ModelAndView("user/comic/list", "comics",
				comicDao.listComics());
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addComic(ModelMap model) {

		model.addAttribute("comic", new Comic());
		model.addAttribute("authorsList", authorDao.listAuthors());

		return "user/comic/add";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String processAddSubmit(@ModelAttribute("comic") Comic comic,
			BindingResult bindingResult, ModelMap model) {

		ComicValidator validator = new ComicValidator();
		validator.validate(comic, bindingResult);
		if (bindingResult.hasErrors()) {
			model.addAttribute("authorsList", authorDao.listAuthors());
			return "user/comic/add";
		}
		comic.setStartOfPublishing(new Date());

		for (Author author : comic.getAuthors()) {
			authorDao.addComic(comic, author.getAuthorId());
		}
		comicDao.addComic(comic);
		return "redirect:list.html";
	}

	@RequestMapping(value = "/update/{code}", method = RequestMethod.GET)
	public ModelAndView updateComic(@PathVariable int code) {
		return new ModelAndView("user/comic/update", "comic",
				comicDao.getComicById(code));
	}

	@RequestMapping(value = "/update/{code}", method = RequestMethod.POST)
	public String processUpdate(@PathVariable int code,
			@ModelAttribute("comic") Comic comic, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			return "user/comic/update";
		}

		comicDao.updateComic(comic);
		return "redirect:../list.html";
	}

	@RequestMapping(value = "/delete/{code}", method = RequestMethod.GET)
	public String deleteComic(@PathVariable int code) {
		comicDao.deleteComic(code);
		return "redirect:../list.html";
	}

}
