package es.uji.ei1027.kirbys.controller;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import es.uji.ei1027.kirbys.model.Comic;

public class ComicValidator implements Validator {

	@Override
	public boolean supports(Class<?> cls) {
		return Comic.class.equals(cls);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		Comic comic = (Comic) obj;
		if (comic.getCode() < 0)
			errors.rejectValue("code", "invalid", "Not a valid integer value");
		if (comic.getTitle().trim().equals(""))
			errors.rejectValue("title", "required", "Must enter a valid title");

		if (comic.getGenre().trim().equals(""))
			errors.rejectValue("genre", "required", "Must enter a valid genre");

		if (comic.getPeriodicity() < 0)
			errors.rejectValue("periodicity", "invalid",
					"Not a valid integer value");

		if (comic.getPublisher() == null)
			errors.rejectValue("publisher", "not found",
					"Can't find a Publisher with that NIF in the system");
		if (comic.getAuthors() == null)
			errors.rejectValue("authors", "required",
					"Must select at least one author for a comic");
	}

}
