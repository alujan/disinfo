package es.uji.ei1027.kirbys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("main/help")
public class HelpController {

	@RequestMapping(value = "/faq", method = RequestMethod.GET)
	public String faqHelp() {
		return "main/help";
	}
}
