package es.uji.ei1027.kirbys.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.uji.ei1027.kirbys.dao.UserDao;
import es.uji.ei1027.kirbys.model.UserDetails;

class UserValidator implements Validator {

	@Override
	public boolean supports(Class<?> cls) {
		return UserDetails.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		// Exercici: Afegeix codi per comprovar que
		// l'usuari i la contrasenya no estiguen buits
		// ...
	}
}

@Controller
public class LoginController {

	@Autowired
	private UserDao userDao;

	@RequestMapping("/login")
	public ModelAndView login() {
		return new ModelAndView("login", "user", new UserDetails());
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String checkLogin(@ModelAttribute("user") UserDetails user,

	BindingResult bindingResult, HttpSession session) {

		// Valida les dades
		UserValidator userValidator = new UserValidator();
		userValidator.validate(user, bindingResult);
		if (bindingResult.hasErrors()) {
			return "login";
		}
		// Comprova que el login siga correcte
		// intentant carregar les dades de l'usuari
		user = userDao.loadUserByUsername(user.getUsername(),
				user.getPassword());
		if (user == null) {
			bindingResult.rejectValue("password", "badpw",
					"Wrong user or password");
			return "login";
		}

		// Autenticats correctament. Guardem les dades de l'usuari autenticat a
		// la sessió
		String nextView = (String)session.getAttribute("nextView");
		session.setAttribute("user", user);
		UserDetails d = new UserDetails();
		d.setUsername(session.getId());
		if(nextView == null)
			return "redirect:index.jsp";
		
		return "redirect:"+nextView;
	}

	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:index.jsp";
	}

}