package es.uji.ei1027.kirbys.controller;

import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.uji.ei1027.kirbys.dao.ComicDao;
import es.uji.ei1027.kirbys.dao.NewCopyDao;
import es.uji.ei1027.kirbys.dao.SecondHandCopyDao;
import es.uji.ei1027.kirbys.model.Comic;
import es.uji.ei1027.kirbys.model.NewCopy;
import es.uji.ei1027.kirbys.model.Publisher;
import es.uji.ei1027.kirbys.model.SecondHandCopy;

@Controller
@RequestMapping("/user/publication")
public class PublicationController {

	@Autowired
	private NewCopyDao newDao;

	@Autowired
	private SecondHandCopyDao sHandDao;

	@Autowired
	private ComicDao comicDao;

	public void setNewDao(NewCopyDao newDao) {
		this.newDao = newDao;
	}

	public void setSHandDao(SecondHandCopyDao sHandDao) {
		this.sHandDao = sHandDao;
	}

	public void setComicDao(ComicDao comicDao) {
		this.comicDao = comicDao;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
		binder.registerCustomEditor(Comic.class, "comic",
				new PropertyEditorSupport() {

					public void setAsText(String incomming) {
						Comic comic = comicDao.getComicById(Integer
								.parseInt(incomming));
						setValue(comic);
					}

					public String getAsText() {
						return String.valueOf(((Comic) getValue()).getCode());
					}

				});
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String listPublications(ModelMap model) {
		model.addAttribute("newcopies", newDao.listNewCopies());
		model.addAttribute("shandcopies", sHandDao.listSecondHandCopies());
		return "user/publication/list";
	}

	@RequestMapping(value = "/addnew", method = RequestMethod.GET)
	public String addNewCopy(ModelMap model) {
		NewCopy copy = new NewCopy();
		copy.setComic(new Comic());
		copy.setIncomeDate(new Date());
		model.addAttribute("newcopy", copy);
		model.addAttribute("comics", comicDao.listComics());
		return "user/publication/addnew";
	}

	@RequestMapping(value = "/addnew", method = RequestMethod.POST)
	public String processAddNewCopy(@ModelAttribute("newcopy") NewCopy newcopy,
			BindingResult bindingResult, ModelMap model) {
		if (bindingResult.hasErrors())
			return "user/publication/addnew";

		if (newDao.exists(newcopy)) {
			bindingResult.rejectValue("number", "notnew",
					" The copy already exists in the system");
			model.addAttribute("comics", comicDao.listComics());
			return "user/publication/addnew";
		}

		newDao.addNewCopy(newcopy);
		comicDao.addPublicationToComic(newcopy);
		return "redirect:../publication.html";
	}

	@RequestMapping(value = "/addshand", method = RequestMethod.GET)
	public String addSecondHandCopy(ModelMap model) {
		SecondHandCopy shandcopy = new SecondHandCopy();
		shandcopy.setComic(new Comic());
		model.addAttribute("shandcopy", shandcopy);
		model.addAttribute("comics", comicDao.listComics());
		return "user/publication/addshand";
	}

	@RequestMapping(value = "/addshand", method = RequestMethod.POST)
	public String processAddSecondHandCopy(
			@ModelAttribute("shandcopy") SecondHandCopy shandcopy,
			BindingResult bindingResult, ModelMap model) {

		if (bindingResult.hasErrors())
			return "user/publication/addshand";

		if (sHandDao.exists(shandcopy)) {
			bindingResult.rejectValue("number", "notnew",
					" The copy already exists in the system");
			model.addAttribute("comics", comicDao.listComics());
			return "user/publication/addshand";
		}
		sHandDao.addSecondHandCopy(shandcopy);
		comicDao.addPublicationToComic(shandcopy);
		return "redirect:../publication.html";
	}

	@RequestMapping(value = "/updatenew/{publicationId}", method = RequestMethod.GET)
	public ModelAndView updateNewCopy(@PathVariable int publicationId) {
		return new ModelAndView("user/publication/updatenew", "newcopy",
				newDao.getNewCopyById(publicationId));
	}

	@RequestMapping(value = "/updatenew/{publicationId}", method = RequestMethod.POST)
	public String processUpdateNew(@PathVariable int publicationId,
			@ModelAttribute("newcopy") NewCopy newcopy,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors())
			return "user/publication/updatenew";
		newDao.updateNewCopy(newcopy);
		return "redirect:../../publication.html";
	}

	@RequestMapping(value = "/updateshand/{publicationId}", method = RequestMethod.GET)
	public ModelAndView updateSecondHandCopy(@PathVariable int publicationId) {
		return new ModelAndView("user/publication/updateshand", "shandcopy",
				sHandDao.getSecondHandCopyById(publicationId));
	}

	@RequestMapping(value = "/updateshand/{publicationId}", method = RequestMethod.POST)
	public String processUpdateSecondHand(@PathVariable int publicationId,
			@ModelAttribute("shandcopy") SecondHandCopy shandcopy,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors())
			return "user/publication/updateshand";
		sHandDao.updateSecondHandCopy(shandcopy);
		return "redirect:../../publication.html";
	}

	@RequestMapping(value = "/deletenew/{publicationId}", method = RequestMethod.GET)
	public String deleteNew(@PathVariable int publicationId) {
		newDao.deleteNewCopy(publicationId);
		return "redirect:../../publication.html";
	}

	@RequestMapping(value = "/deleteshand/{publicationId}", method = RequestMethod.GET)
	public String deleteSecondHandCopy(@PathVariable int publicationId) {
		sHandDao.deleteSecondHandCopy(publicationId);
		return "redirect:../../publication.html";
	}

}
