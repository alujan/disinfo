package es.uji.ei1027.kirbys.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.uji.ei1027.kirbys.dao.PublisherDao;
import es.uji.ei1027.kirbys.model.Publisher;

@Controller
@RequestMapping("/user/publisher")
public class PublisherController {

	@Autowired
	private PublisherDao publisherDao;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView listPublishers() {
		return new ModelAndView("user/publisher/list", "publishers",
				publisherDao.listPublishers());
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView addPublisher() {
		return new ModelAndView("user/publisher/add", "publisher",
				new Publisher());
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String processAdd(@ModelAttribute("publisher") Publisher publisher,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors())
			return "user/publisher/add";

		publisherDao.addPublisher(publisher);
		return "redirect:../publisher.html";

	}

	@RequestMapping(value = "/update/{publisherId}", method = RequestMethod.GET)
	public ModelAndView updatePublisher(@PathVariable int publisherId) {
		return new ModelAndView("user/publisher/update", "publisher",
				publisherDao.getPublisherById(publisherId));

	}

	@RequestMapping(value = "/update/{publisherId}", method = RequestMethod.POST)
	public String processUpdate(@PathVariable int publisherId,
			@ModelAttribute("publisher") Publisher publisher,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return "user/publisher/update";
		publisherDao.updatePublisher(publisher);
		return "redirect:../../publisher.html";
	}

	@RequestMapping(value = "/delete/{publisherId}", method = RequestMethod.GET)
	public String deletePublisher(@PathVariable int publisherId, ModelMap model) {
		if (publisherDao.hasComics(publisherId)) {
			model.addAttribute("message",
					"This publisher has a Comic and cannot be deleted");
			model.addAttribute("publishers", publisherDao.listPublishers());
			return "user/publisher/list";
		}
		publisherDao.deletePublisher(publisherId);
		return "redirect:../../publisher.html";

	}
}
