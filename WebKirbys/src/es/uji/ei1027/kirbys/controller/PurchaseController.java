package es.uji.ei1027.kirbys.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uji.ei1027.kirbys.dao.PurchaseDao;
import es.uji.ei1027.kirbys.model.Purchase;
import es.uji.ei1027.kirbys.model.SecondHandCopy;

@Controller
@RequestMapping("/user/purchase")
public class PurchaseController {

	@Autowired
	PurchaseDao purchaseDao;

	public void setPurchaseDao(PurchaseDao purchaseDao) {
		this.purchaseDao = purchaseDao;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String purchasesList(HttpSession session) {
		return "user/purchase/search";
	}

	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String purchaseDetail(HttpServletRequest req, ModelMap model) {

		int code = Integer.parseInt((String) req.getParameter("filter"));
		List<SecondHandCopy> copies = purchaseDao.getPurchaseCopies(code);
		model.addAttribute("purchase", purchaseDao.getPurchaseById(code));
		model.addAttribute("copies", copies);
		return "user/purchase/details";
	}
}
