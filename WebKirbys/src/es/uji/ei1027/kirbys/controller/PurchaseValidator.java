package es.uji.ei1027.kirbys.controller;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import es.uji.ei1027.kirbys.model.SecondHandCopy;

public class PurchaseValidator implements Validator {

	@Override
	public boolean supports(Class<?> cls) {
		return SecondHandCopy.class.equals(cls);
	}

	@SuppressWarnings("unused")
	@Override
	public void validate(Object obj, Errors errors) {
		SecondHandCopy shandcopy = (SecondHandCopy) obj;

	}
}
