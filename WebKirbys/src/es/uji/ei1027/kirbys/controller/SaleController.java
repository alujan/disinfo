package es.uji.ei1027.kirbys.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uji.ei1027.kirbys.dao.SaleDao;
import es.uji.ei1027.kirbys.model.Publication;

@Controller
@RequestMapping("/user/sale")
public class SaleController {

	@Autowired
	SaleDao saleDao;

	public void setSaelDao(SaleDao saleDao) {
		this.saleDao = saleDao;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String purchasesList(HttpSession session) {
		return "user/sale/search";
	}

	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String purchaseDetail(HttpServletRequest req, ModelMap model) {

		int code = Integer.parseInt((String) req.getParameter("filter"));
		List<Publication> copies = saleDao.getSaleCopies(code);
		model.addAttribute("sale", saleDao.getSaleById(code));
		model.addAttribute("copies", copies);
		return "user/sale/details";
	}

}
