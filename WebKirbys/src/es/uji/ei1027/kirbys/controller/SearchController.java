package es.uji.ei1027.kirbys.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.uji.ei1027.kirbys.dao.AuthorDao;
import es.uji.ei1027.kirbys.dao.ComicDao;
import es.uji.ei1027.kirbys.dao.NewCopyDao;
import es.uji.ei1027.kirbys.dao.SecondHandCopyDao;
import es.uji.ei1027.kirbys.model.Comic;
import es.uji.ei1027.kirbys.model.SecondHandCopy;

@Controller
@RequestMapping("/main/search")
public class SearchController {

	@Autowired
	private ComicDao comicDao;

	@Autowired
	private AuthorDao authorDao;

	@Autowired
	private SecondHandCopyDao sHandDao;

	@Autowired
	private NewCopyDao newCopyDao;

	public void setComicDao(ComicDao comicDao) {
		this.comicDao = comicDao;
	}

	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	public void setsHandDao(SecondHandCopyDao sHandDao) {
		this.sHandDao = sHandDao;
	}

	public void setNewCopyDao(NewCopyDao newCopyDao) {
		this.newCopyDao = newCopyDao;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@RequestMapping(value = "/item")
	public String startSearch() {
		return "main/search";
	}

	@RequestMapping(value = "/filter", method = RequestMethod.GET)
	public ModelAndView filter(HttpServletRequest req) {

		String option = (String) req.getParameter("option");
		String filter = (String) req.getParameter("filter");

		List<Comic> results = null;
		if (option.equals("code"))
			results = comicDao.searchByCode(filter);
		if (option.equals("author"))
			results = comicDao.searchByAuthor(filter);
		if (option.equals("title"))
			results = comicDao.searchByTitle(filter);
		if (option.equals("publisher"))
			results = comicDao.searchByPublisher(filter);
		if (option.equals("genre"))
			results = comicDao.searchByGenre(filter);
		return new ModelAndView("main/search", "comics", results);
	}

	@RequestMapping(value = "/details/{code}", method = RequestMethod.GET)
	public String details(@PathVariable int code, ModelMap model) {

		model.addAttribute("newcopies", newCopyDao.getComicCopies(code));
		model.addAttribute("shandcopies", sHandDao.getComicCopies(code));
		model.addAttribute("comic", comicDao.getComicById(code));
		return "main/details";
	}
}
