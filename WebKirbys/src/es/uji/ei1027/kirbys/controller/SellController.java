package es.uji.ei1027.kirbys.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.uji.ei1027.kirbys.dao.NewCopyDao;
import es.uji.ei1027.kirbys.dao.PublicationDao;
import es.uji.ei1027.kirbys.dao.SaleDao;
import es.uji.ei1027.kirbys.dao.SecondHandCopyDao;
import es.uji.ei1027.kirbys.model.Comic;
import es.uji.ei1027.kirbys.model.NewCopy;
import es.uji.ei1027.kirbys.model.Publication;
import es.uji.ei1027.kirbys.model.Purchase;
import es.uji.ei1027.kirbys.model.Sale;
import es.uji.ei1027.kirbys.model.SecondHandCopy;

@Controller
@RequestMapping("/main/sell")
public class SellController {

	@Autowired
	private SaleDao saleDao;

	@Autowired
	private NewCopyDao newDao;

	@Autowired
	private SecondHandCopyDao sHandDao;

	@Autowired
	private PublicationDao pubDao;

	public void setPubDao(PublicationDao pubDao) {
		this.pubDao = pubDao;
	}

	public void setSaleDao(SaleDao saleDao) {
		this.saleDao = saleDao;
	}

	public void setSecondHandCopyDao(SecondHandCopyDao sHandDao) {
		this.sHandDao = sHandDao;
	}

	public void setNewCopyDao(NewCopyDao newCopyDao) {
		this.newDao = newCopyDao;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String listComics(HttpSession session) {
		if (session.getAttribute("checklist") == null) {
			session.setAttribute("checklist", new LinkedList<Publication>());
			session.setAttribute("total", new Double(0.0));
		}
		return "main/sell";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/sale", method = RequestMethod.POST)
	public String addPurchase(ModelMap model, HttpServletRequest req,
			HttpSession session) {

		Publication toSell;
		int number = Integer.parseInt(req.getParameter("number"));
		int status = Integer.parseInt(req.getParameter("status"));
		int code = Integer.parseInt(req.getParameter("code"));
		boolean used = Boolean.parseBoolean(req.getParameter("used"));

		double total = (Double) session.getAttribute("total");
		List<Publication> checklist = (List<Publication>) session
				.getAttribute("checklist");

		if (!newDao.exists(number, code)) {
			model.addAttribute("message",
					"The number of the copy does not exist in the system");
			return "main/sell";

		} else if (used) {
			toSell = sHandDao.getSecondHandCopyByParams(number, code, status);
		} else {
			toSell = newDao.getNewCopyByParams(number, code);
		}

		total += toSell.getPrice();
		total = Math.rint(total * 100) / 100;
		checklist.add(toSell);
		session.setAttribute("checklist", checklist);
		session.setAttribute("total", total);
		return "redirect:../sell.html";

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/sale/{publicationId}", method = RequestMethod.GET)
	public String addPurchase(@PathVariable int publicationId,
			HttpSession session) {

		Publication toSell = pubDao.getPublicationById(publicationId);
		List<Publication> checklist = (List<Publication>) session
				.getAttribute("checklist");
		double total = (Double) session.getAttribute("total");
		
		if (checklist == null) {
			checklist = new LinkedList<Publication>();
			session.setAttribute("checklist", checklist);
			session.setAttribute("total", new Double(0.0));
		}

		checklist.add(toSell);
		total += toSell.getPrice();
		total = Math.rint(total * 100) / 100;
		session.setAttribute("checklist", checklist);
		session.setAttribute("total", total);

		return "redirect:../../sell.html";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/delete/{publicationId}", method = RequestMethod.GET)
	public String delete(@PathVariable int publicationId, HttpSession session) {

		List<Publication> checklist = (List<Publication>) session
				.getAttribute("checklist");
		double total = (Double) session.getAttribute("total");

		Publication toDelete = pubDao.getPublicationById(publicationId);
		System.out.println("AQUI ENTROOO");
		total -= toDelete.getPrice();
		System.out.println("AQUI TAMBIEN");
		total = Math.rint(total * 100) / 100;
		checklist = this.delteCopy(checklist, toDelete);
		session.setAttribute("checklist", checklist);
		session.setAttribute("total", total);
		return "redirect:../../sell.html";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/checkout", method = RequestMethod.GET)
	public String checkout(HttpSession session) {

		List<Publication> checklist = (List<Publication>) session
				.getAttribute("checklist");
		double total = (Double) session.getAttribute("total");

		Sale sale = saleDao.createSale();
		List<Publication> insale = new LinkedList<Publication>();

		for (Publication copy : checklist) {
			insale.add(copy);
			pubDao.doPurchase(copy, sale);
		}

		sale.setPublications(insale);
		sale.setTotal(total);
		saleDao.updateSale(sale);
		session.setAttribute("checklist", new LinkedList<SecondHandCopy>());
		session.setAttribute("total", new Double(0.0));
		return "main/saleconfirm";
	}

	private List<Publication> delteCopy(List<Publication> list,
			Publication toDelete) {

		for (Publication copy : list) {
			if (copy.getPublicationId() == toDelete.getPublicationId()) {
				toDelete = copy;
				break;
			}
		}
		list.remove(toDelete);
		return list;
	}

}
