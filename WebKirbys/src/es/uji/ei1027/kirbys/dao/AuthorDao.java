package es.uji.ei1027.kirbys.dao;

import java.util.List;

import es.uji.ei1027.kirbys.model.Author;
import es.uji.ei1027.kirbys.model.Comic;

public interface AuthorDao {

	List<Author> listAuthors();

	Author getAuthorById(int authorId);

	void addAuthor(Author author);

	void updateAuthor(Author author);

	void deleteAuthor(int authorId);

	void addComic(Comic comic, int authorId);
}
