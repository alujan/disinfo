package es.uji.ei1027.kirbys.dao;

import java.util.List;

import es.uji.ei1027.kirbys.model.Comic;
import es.uji.ei1027.kirbys.model.Publication;

public interface ComicDao {

	List<Comic> listComics();

	Comic getComicById(int code);

	void addComic(Comic comic);

	void updateComic(Comic comic);

	void deleteComic(int code);

	List<Comic> searchByCode(String filter);

	List<Comic> searchByAuthor(String filter);

	List<Comic> searchByTitle(String filter);

	List<Comic> searchByPublisher(String filter);

	List<Comic> searchByGenre(String filter);

	void addPublicationToComic(Publication copy);
}
