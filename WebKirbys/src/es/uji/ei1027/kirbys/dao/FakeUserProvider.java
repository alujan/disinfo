package es.uji.ei1027.kirbys.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.jasypt.util.password.BasicPasswordEncryptor;
import org.springframework.stereotype.Repository;

import es.uji.ei1027.kirbys.model.UserDetails;

@Repository
public class FakeUserProvider implements UserDao {
	final Map<String, UserDetails> knownUsers = new HashMap<String, UserDetails>();

	public FakeUserProvider() {
		BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
		UserDetails userAlice = new UserDetails();
		userAlice.setUsername("alejandro");
		userAlice.setPassword(passwordEncryptor.encryptPassword("alejandro"));
		knownUsers.put("alejandro", userAlice);

		UserDetails userBob = new UserDetails();
		userBob.setUsername("hector");
		userBob.setPassword(passwordEncryptor.encryptPassword("hector"));
		knownUsers.put("hector", userBob);

		UserDetails userPepe = new UserDetails();
		userPepe.setUsername("fernando");
		userPepe.setPassword(passwordEncryptor.encryptPassword("fernando"));
		knownUsers.put("fernando", userPepe);
	}

	@Override
	public UserDetails loadUserByUsername(String username, String password) {
		UserDetails user = knownUsers.get(username.trim());
		if (user == null)
			return null; // Usuari no trobat
		// Contrasenya
		BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
		if (passwordEncryptor.checkPassword(password, user.getPassword())) {
			return user;
		} else {
			return null; // bad login!
		}
	}

	@Override
	public Collection<UserDetails> listAllUsers() {
		return knownUsers.values();
	}

}