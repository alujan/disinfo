package es.uji.ei1027.kirbys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.ei1027.kirbys.model.Author;
import es.uji.ei1027.kirbys.model.Comic;

@Repository
public class HibernateAuthorDao implements AuthorDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Author> listAuthors() {
		return getCurrentSession().createQuery("from Author").list();
	}

	@Override
	@Transactional
	public Author getAuthorById(int authorId) {
		Query query = getCurrentSession().createQuery(
				"from Author where authorId=:authorId");
		query.setParameter("authorId", authorId);
		return (Author) query.uniqueResult();
	}

	@Override
	@Transactional
	public void addAuthor(Author author) {
		getCurrentSession().save(author);
	}

	@Override
	@Transactional
	public void updateAuthor(Author author) {
		getCurrentSession().update(author);
	}

	@Override
	@Transactional
	public void deleteAuthor(int authorId) {
		getCurrentSession().delete(getAuthorById(authorId));
	}

	@Override
	@Transactional
	public void addComic(Comic comic, int authorId) {
		Author author = getAuthorById(authorId);
		author.getComics().add(comic);
		updateAuthor(author);
	}

}
