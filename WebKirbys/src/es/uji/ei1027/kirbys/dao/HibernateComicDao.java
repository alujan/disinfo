package es.uji.ei1027.kirbys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.ei1027.kirbys.model.Comic;
import es.uji.ei1027.kirbys.model.Publication;

@Repository
public class HibernateComicDao implements ComicDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Comic> listComics() {
		return getCurrentSession().createQuery("from Comic").list();
	}

	@Override
	@Transactional
	public Comic getComicById(int code) {
		Query query = getCurrentSession().createQuery(
				"from Comic where code=:code");
		query.setParameter("code", code);
		return (Comic) query.uniqueResult();
	}

	@Override
	@Transactional
	public void addComic(Comic comic) {
		getCurrentSession().save(comic);
	}

	@Override
	@Transactional
	public void updateComic(Comic comic) {
		getCurrentSession().update(comic);
	}

	@Override
	@Transactional
	public void deleteComic(int code) {
		getCurrentSession().delete(getComicById(code));
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Comic> searchByCode(String filter) {
		Query query = getCurrentSession().createQuery(
				"from Comic where str(code) like :code order by title");
		query.setParameter("code", filter + "%");
		List<Comic> results = query.list();
		return results;
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Comic> searchByAuthor(String filter) {

		Query query = getCurrentSession().createQuery(
				"select c from Comic.authors where name=:name");
		query.setParameter("name", "%" + filter + "%");
		List<Comic> results = query.list();
		return results;
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Comic> searchByTitle(String filter) {
		Query query = getCurrentSession().createQuery(
				"from Comic where title like :title order by title");
		query.setParameter("title", "%" + filter + "%");
		List<Comic> results = query.list();
		return results;

	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Comic> searchByPublisher(String filter) {
		Query query = getCurrentSession().createQuery(
				"from Comic as c where c.publisher.nif=:nif order by title");
		query.setParameter("nif", filter);
		List<Comic> results = query.list();
		return results;
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<Comic> searchByGenre(String filter) {
		Query query = getCurrentSession().createQuery(
				"from Comic where genre like :genre order by title");
		query.setParameter("genre", "%" + filter + "%");
		List<Comic> results = query.list();
		return results;
	}

	@Override
	@Transactional
	public void addPublicationToComic(Publication copy) {
		Comic comic = getComicById(copy.getComic().getCode());
		comic.getPublications().add(copy);
		updateComic(comic);

	}
}
