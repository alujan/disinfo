package es.uji.ei1027.kirbys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.ei1027.kirbys.model.Comic;
import es.uji.ei1027.kirbys.model.NewCopy;
import es.uji.ei1027.kirbys.model.Sale;

@Repository
public class HibernateNewCopyDao implements NewCopyDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<NewCopy> listNewCopies() {
		return getCurrentSession().createQuery("from NewCopy").list();
	}

	@Override
	@Transactional
	public NewCopy getNewCopyById(int publicationId) {
		Query query = getCurrentSession().createQuery(
				"from NewCopy where publicationId=:publicationId");
		query.setParameter("publicationId", publicationId);
		return (NewCopy) query.uniqueResult();
	}

	@Override
	@Transactional
	public void addNewCopy(NewCopy copy) {
		getCurrentSession().save(copy);
	}

	@Override
	@Transactional
	public void updateNewCopy(NewCopy copy) {
		getCurrentSession().update(copy);
	}

	@Override
	@Transactional
	public void deleteNewCopy(int publicationId) {
		getCurrentSession().delete(getNewCopyById(publicationId));
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<NewCopy> getComicCopies(int code) {
		Query query = getCurrentSession().createQuery(
				"from NewCopy where comic.code=:code order by publicationId");
		query.setParameter("code", code);
		List<NewCopy> results = query.list();
		return results;
	}

	@Override
	@Transactional
	public boolean exists(NewCopy copy) {
		Query query = getCurrentSession().createQuery(
				"from NewCopy where number=:number and comic.code=:code");
		query.setParameter("number", copy.getNumber());
		query.setParameter("code", copy.getComic().getCode());
		return (query.uniqueResult() != null);
	}

	@Override
	@Transactional
	public boolean exists(int number, int code) {
		Query query = getCurrentSession().createQuery(
				"from NewCopy where number=:number and comic.code=:code");
		query.setParameter("number", number);
		query.setParameter("code", code);
		return (query.uniqueResult() != null);
	}

	@Override
	@Transactional
	public NewCopy getNewCopyByParams(int number, int code) {
		Query query = getCurrentSession().createQuery(
				"from NewCopy where number=:number and comic.code=:code");
		query.setParameter("number", number);
		query.setParameter("code", code);
		return (NewCopy) query.uniqueResult();
	}

}
