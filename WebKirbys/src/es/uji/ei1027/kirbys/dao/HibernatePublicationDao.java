package es.uji.ei1027.kirbys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.ei1027.kirbys.model.NewCopy;
import es.uji.ei1027.kirbys.model.Publication;
import es.uji.ei1027.kirbys.model.Purchase;
import es.uji.ei1027.kirbys.model.Sale;
import es.uji.ei1027.kirbys.model.SecondHandCopy;

@Repository
public class HibernatePublicationDao implements PublicationDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Publication> listPublications() {
		return getCurrentSession().createQuery("from Publication").list();
	}

	@Override
	@Transactional
	public Publication getPublicationById(int publicationId) {
		Query query = getCurrentSession().createQuery(
				"from Publication where publicationId=:publicationId");
		query.setParameter("publicationId", publicationId);
		return (Publication) query.uniqueResult();
	}

	@Override
	@Transactional
	public void addPublication(Publication pub) {
		getCurrentSession().save(pub);
	}

	@Override
	@Transactional
	public void updatePublication(Publication pub) {
		getCurrentSession().update(pub);
	}

	@Override
	public void deletePublication(int publicationId) {
		getCurrentSession().delete(getPublicationById(publicationId));
	}

	@Override
	@Transactional
	public void doSale(Publication copy, Sale sale) {
		copy = getPublicationById(copy.getNumber());
		int stock = copy.getStock();
		stock--;
		copy.setStock(stock);
		copy.getSales().add(sale);
		updatePublication(copy);
	}

	@Override
	@Transactional
	public void doPurchase(Publication copy, Sale sale) {
		copy = getPublicationById(copy.getPublicationId());
		int stock = copy.getStock();
		stock--;
		copy.setStock(stock);
		copy.getSales().add(sale);
		updatePublication(copy);
	}

}
