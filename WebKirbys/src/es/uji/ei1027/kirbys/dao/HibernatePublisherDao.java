package es.uji.ei1027.kirbys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.ei1027.kirbys.model.Publisher;

@Repository
public class HibernatePublisherDao implements PublisherDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Publisher> listPublishers() {
		return getCurrentSession().createQuery("from Publisher").list();
	}

	@Override
	@Transactional
	public Publisher getPublisherById(int publisherId) {
		Query query = getCurrentSession().createQuery(
				"from Publisher where publisherId=:publisherId");
		query.setParameter("publisherId", publisherId);
		return (Publisher) query.uniqueResult();
	}

	@Override
	@Transactional
	public Publisher getPublisherByNIF(String nif) {
		Query query = getCurrentSession().createQuery(
				"from Publisher where nif=:nif");
		query.setParameter("nif", nif);
		return (Publisher) query.uniqueResult();
	}

	@Override
	@Transactional
	public void addPublisher(Publisher publisher) {
		getCurrentSession().save(publisher);
	}

	@Override
	@Transactional
	public void updatePublisher(Publisher publisher) {
		getCurrentSession().update(publisher);

	}

	@Override
	@Transactional
	public void deletePublisher(int publisherId) {
		getCurrentSession().delete(getPublisherById(publisherId));
	}

	@Override
	@Transactional
	public boolean hasComics(int publisherId) {
		return getPublisherById(publisherId).getComics().size() != 0;
	}

}
