package es.uji.ei1027.kirbys.dao;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.ei1027.kirbys.model.Purchase;
import es.uji.ei1027.kirbys.model.SecondHandCopy;

@Repository
public class HibernatePurchaseDao implements PurchaseDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private SecondHandCopyDao sHandDao;

	public void setsHandDao(SecondHandCopyDao sHandDao) {
		this.sHandDao = sHandDao;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Purchase> listPurchases() {
		return getCurrentSession().createQuery("from Purchase order by code")
				.list();
	}

	@Override
	@Transactional
	public Purchase getPurchaseById(int code) {
		Query query = getCurrentSession().createQuery(
				"from Purchase where code=:code");
		query.setParameter("code", code);
		return (Purchase) query.uniqueResult();
	}

	@Override
	@Transactional
	public void addPurchase(Purchase purchase) {
		getCurrentSession().save(purchase);
	}

	@Override
	@Transactional
	public void updatePurchase(Purchase purchase) {
		getCurrentSession().update(purchase);

	}

	@Override
	@Transactional
	public void deletePurchase(int code) {
		getCurrentSession().delete(getPurchaseById(code));
	}

	@Override
	@Transactional
	public Purchase createPurchase() {

		Purchase purchase = new Purchase();
		purchase.setsHandCopies(new LinkedList<SecondHandCopy>());
		purchase.setDate(new Date());
		purchase.setTotal(new Double(0.0));
		addPurchase(purchase);
		List<Purchase> purchases = listPurchases();
		return purchases.get(purchases.size() - 1);
	}

	@Override
	@Transactional
	public List<SecondHandCopy> getPurchaseCopies(int code) {
		return (List<SecondHandCopy>) getPurchaseById(code).getsHandCopies();
	}
}
