package es.uji.ei1027.kirbys.dao;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.ei1027.kirbys.model.Publication;
import es.uji.ei1027.kirbys.model.Sale;

@Repository
public class HibernateSaleDao implements SaleDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Sale> listSales() {
		return getCurrentSession().createQuery("from Sale order by code")
				.list();
	}

	@Override
	@Transactional
	public Sale getSaleById(int code) {
		Query query = getCurrentSession().createQuery(
				"from Sale where code=:code");
		query.setParameter("code", code);
		return (Sale) query.uniqueResult();
	}

	@Override
	@Transactional
	public void addSale(Sale sale) {
		getCurrentSession().save(sale);
	}

	@Override
	@Transactional
	public void updateSale(Sale sale) {
		getCurrentSession().update(sale);
	}

	@Override
	@Transactional
	public void deleteSale(int code) {
		getCurrentSession().delete(getSaleById(code));
	}

	@Override
	@Transactional
	public Sale createSale() {

		Sale sale = new Sale();
		sale.setPublications(new LinkedList<Publication>());
		sale.setDate(new Date());
		sale.setTotal(new Double(0.0));
		addSale(sale);
		List<Sale> sales = listSales();
		return sales.get(sales.size() - 1);
	}

	@Override
	@Transactional
	public List<Publication> getSaleCopies(int code) {
		return (List<Publication>) getSaleById(code).getPublications();
	}
}
