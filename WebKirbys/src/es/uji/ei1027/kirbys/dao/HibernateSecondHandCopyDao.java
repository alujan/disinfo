package es.uji.ei1027.kirbys.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.ei1027.kirbys.model.Comic;
import es.uji.ei1027.kirbys.model.NewCopy;
import es.uji.ei1027.kirbys.model.Purchase;
import es.uji.ei1027.kirbys.model.SecondHandCopy;

@Repository
public class HibernateSecondHandCopyDao implements SecondHandCopyDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<SecondHandCopy> listSecondHandCopies() {
		return getCurrentSession().createQuery("from SecondHandCopy").list();
	}

	@Override
	@Transactional
	public SecondHandCopy getSecondHandCopyById(int publicationId) {
		Query query = getCurrentSession().createQuery(
				"from SecondHandCopy where publicationId=:publicationId");
		query.setParameter("publicationId", publicationId);
		return (SecondHandCopy) query.uniqueResult();
	}

	@Override
	@Transactional
	public void addSecondHandCopy(SecondHandCopy copy) {
		getCurrentSession().save(copy);
	}

	@Override
	@Transactional
	public void updateSecondHandCopy(SecondHandCopy copy) {
		getCurrentSession().update(copy);

	}

	@Override
	@Transactional
	public void deleteSecondHandCopy(int publicationId) {
		getCurrentSession().delete(getSecondHandCopyById(publicationId));
	}

	@Override
	@Transactional
	public List<Purchase> loadPurchases(int publicationId) {

		List<Purchase> purchases = (List<Purchase>) getSecondHandCopyById(
				publicationId).getPurchases();
		return purchases;
	}

	@Override
	@Transactional
	public void doPurchase(SecondHandCopy shandcopy, Purchase purchase) {

		shandcopy = getSecondHandCopyById(shandcopy.getPublicationId());
		int stock = shandcopy.getStock();
		stock++;
		shandcopy.setStock(stock);
		shandcopy.getPurchases().add(purchase);
		updateSecondHandCopy(shandcopy);
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<SecondHandCopy> getComicCopies(int code) {
		Query query = getCurrentSession().createQuery(
				"from SecondHandCopy where comic.code=:code order by number");
		query.setParameter("code", code);
		List<SecondHandCopy> results = query.list();
		return results;
	}

	@Override
	@Transactional
	public boolean exists(SecondHandCopy copy) {
		Query query = getCurrentSession()
				.createQuery(
						"from SecondHandCopy where number=:number and comic.code=:code and preservationStatus=:preservationStatus");
		query.setParameter("number", copy.getNumber());
		query.setParameter("code", copy.getComic().getCode());
		query.setParameter("preservationStatus", copy.getPreservationStatus());
		return (query.uniqueResult() != null);
	}

	@Override
	@Transactional
	public boolean exists(int number, int code, int status) {
		Query query = getCurrentSession()
				.createQuery(
						"from SecondHandCopy where number=:number and comic.code=:code and preservationStatus=:preservationStatus");
		query.setParameter("number", number);
		query.setParameter("code", code);
		query.setParameter("preservationStatus", status);
		return (query.uniqueResult() != null);
	}

	@Override
	@Transactional
	public SecondHandCopy getSecondHandCopyByParams(int number, int code,
			int status) {
		Query query = getCurrentSession()
				.createQuery(
						"from SecondHandCopy where number=:number and comic.code=:code and preservationStatus=:preservationStatus");
		query.setParameter("number", number);
		query.setParameter("code", code);
		query.setParameter("preservationStatus", status);
		return (SecondHandCopy) query.uniqueResult();
	}

}
