package es.uji.ei1027.kirbys.dao;

import java.util.List;

import es.uji.ei1027.kirbys.model.NewCopy;
import es.uji.ei1027.kirbys.model.Sale;

public interface NewCopyDao {

	List<NewCopy> listNewCopies();

	NewCopy getNewCopyById(int publicationId);

	boolean exists(NewCopy copy);

	void addNewCopy(NewCopy copy);

	void updateNewCopy(NewCopy copy);

	void deleteNewCopy(int publicationId);

	List<NewCopy> getComicCopies(int code);

	boolean exists(int number, int code);

	NewCopy getNewCopyByParams(int number, int code);
}
