package es.uji.ei1027.kirbys.dao;

import java.util.List;

import es.uji.ei1027.kirbys.model.Publication;
import es.uji.ei1027.kirbys.model.Sale;

public interface PublicationDao {

	List<Publication> listPublications();

	Publication getPublicationById(int publicationId);

	void addPublication(Publication pub);

	void updatePublication(Publication pub);

	void deletePublication(int publicationId);

	void doSale(Publication copy, Sale sale);

	void doPurchase(Publication copy, Sale sale);

}
