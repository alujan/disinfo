package es.uji.ei1027.kirbys.dao;

import java.util.List;

import es.uji.ei1027.kirbys.model.Publisher;

public interface PublisherDao {

	List<Publisher> listPublishers();

	Publisher getPublisherById(int publisherId);

	void addPublisher(Publisher publisher);

	void updatePublisher(Publisher publisher);

	void deletePublisher(int publisherId);

	Publisher getPublisherByNIF(String nif);

	boolean hasComics(int publisherId);
}
