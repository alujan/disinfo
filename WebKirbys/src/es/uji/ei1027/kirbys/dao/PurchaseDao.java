package es.uji.ei1027.kirbys.dao;

import java.util.List;

import es.uji.ei1027.kirbys.model.Purchase;
import es.uji.ei1027.kirbys.model.SecondHandCopy;

public interface PurchaseDao {

	List<Purchase> listPurchases();

	Purchase getPurchaseById(int code);

	void addPurchase(Purchase purchase);

	void updatePurchase(Purchase purchase);

	void deletePurchase(int code);

	Purchase createPurchase();

	List<SecondHandCopy> getPurchaseCopies(int code);
}
