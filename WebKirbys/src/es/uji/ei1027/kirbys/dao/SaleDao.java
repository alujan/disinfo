package es.uji.ei1027.kirbys.dao;

import java.util.List;

import es.uji.ei1027.kirbys.model.Publication;
import es.uji.ei1027.kirbys.model.Sale;

public interface SaleDao {

	List<Sale> listSales();

	Sale getSaleById(int code);

	void addSale(Sale sale);

	void updateSale(Sale sale);

	void deleteSale(int code);

	Sale createSale();

	List<Publication> getSaleCopies(int code);
}
