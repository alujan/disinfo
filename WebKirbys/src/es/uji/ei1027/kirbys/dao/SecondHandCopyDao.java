package es.uji.ei1027.kirbys.dao;

import java.util.List;

import es.uji.ei1027.kirbys.model.Comic;
import es.uji.ei1027.kirbys.model.Purchase;
import es.uji.ei1027.kirbys.model.SecondHandCopy;

public interface SecondHandCopyDao {

	List<SecondHandCopy> listSecondHandCopies();

	SecondHandCopy getSecondHandCopyById(int publicationId);

	boolean exists(SecondHandCopy copy);

	void addSecondHandCopy(SecondHandCopy copy);

	void updateSecondHandCopy(SecondHandCopy copy);

	void deleteSecondHandCopy(int publicationId);

	List<Purchase> loadPurchases(int publicationId);

	void doPurchase(SecondHandCopy shandcopy, Purchase purchase);

	List<SecondHandCopy> getComicCopies(int code);

	boolean exists(int number, int code, int status);

	SecondHandCopy getSecondHandCopyByParams(int number, int code, int status);

}
