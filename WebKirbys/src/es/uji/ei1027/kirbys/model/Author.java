package es.uji.ei1027.kirbys.model;

import java.util.LinkedHashSet;
import java.util.Set;

public class Author {

	private int authorId;
	private String name;
	private String pseudonym;
	private Set <Comic> comics = new LinkedHashSet<Comic>();
	
	public int getAuthorId() {
		return authorId;
	}
	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPseudonym() {
		return pseudonym;
	}
	public void setPseudonym(String pseudonym) {
		this.pseudonym = pseudonym;
	}
	public Set<Comic> getComics() {
		return comics;
	}
	public void setComics(Set<Comic> comics) {
		this.comics = comics;
	}
	
}
