package es.uji.ei1027.kirbys.model;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

public class Comic {

	private int code;
	private String title;
	private int periodicity;
	private String genre;
	private Date startOfPublishing;
	private Date endOfPublishing;
	private Publisher publisher = new Publisher();
	private Set<Author> authors = new LinkedHashSet<Author>();
	private Set<Publication> publications = new LinkedHashSet<Publication>();

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPeriodicity() {
		return periodicity;
	}

	public void setPeriodicity(int periodicity) {
		this.periodicity = periodicity;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Date getStartOfPublishing() {
		return startOfPublishing;
	}

	public void setStartOfPublishing(Date startOfPublishing) {
		this.startOfPublishing = startOfPublishing;
	}

	public Date getEndOfPublishing() {
		return endOfPublishing;
	}

	public void setEndOfPublishing(Date endOfPublishing) {
		this.endOfPublishing = endOfPublishing;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public Set<Publication> getPublications() {
		return publications;
	}

	public void setPublications(Set<Publication> publications) {
		this.publications = publications;
	}

}
