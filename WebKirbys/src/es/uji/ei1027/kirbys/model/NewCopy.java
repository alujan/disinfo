package es.uji.ei1027.kirbys.model;

import java.util.Date;

public class NewCopy extends Publication {

	private Date incomeDate;

	public Date getIncomeDate() {
		return incomeDate;
	}

	public void setIncomeDate(Date incomeDate) {
		this.incomeDate = incomeDate;
	}

}
