package es.uji.ei1027.kirbys.model;

import java.util.LinkedHashSet;
import java.util.Set;

public class Publisher {
	
	private int publisherId;
	private String nif;
	private String address;
	private int telephone;
	private Set<Comic> comics = new LinkedHashSet<Comic>();

	
	public int getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(int publisherId) {
		this.publisherId = publisherId;
	}

	public Set<Comic> getComics() {
		return comics;
	}

	public void setComics(Set<Comic> comics) {
		this.comics = comics;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}

}
