package es.uji.ei1027.kirbys.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Purchase {

	private int code;
	private Date date;
	private double total;
	private List<SecondHandCopy> sHandCopies = new LinkedList<SecondHandCopy>();

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public List<SecondHandCopy> getsHandCopies() {
		return sHandCopies;
	}

	public void setsHandCopies(List<SecondHandCopy> sHandCopies) {
		this.sHandCopies = sHandCopies;
	}

}
