package es.uji.ei1027.kirbys.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Sale {

	private int code;
	private Date date;
	private double total;
	private List<Publication> publications = new ArrayList<Publication>();

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public List<Publication> getPublications() {
		return publications;
	}

	public void setPublications(List<Publication> publications) {
		this.publications = publications;
	}

}
