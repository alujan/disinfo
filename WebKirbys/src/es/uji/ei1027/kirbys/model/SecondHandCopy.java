package es.uji.ei1027.kirbys.model;

import java.util.LinkedList;
import java.util.List;

public class SecondHandCopy extends Publication {

	private int preservationStatus;
	private List<Purchase> purchases = new LinkedList<Purchase>();

	public int getPreservationStatus() {
		return preservationStatus;
	}

	public void setPreservationStatus(int preservationStatus) {
		this.preservationStatus = preservationStatus;
	}

	public List<Purchase> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<Purchase> purchases) {
		this.purchases = purchases;
	}

}